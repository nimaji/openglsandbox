#version 440 core

layout(location = 0) in vec3 vertex;

layout(location = 0) uniform mat4 viewProjection;

out vec4 vs_color;

const vec4 colorA = vec4(0, 1, 1, 1);
const vec4 colorB = vec4(1, 1, 0, 1);

void main()
{
	gl_Position = viewProjection * vec4(vertex, 1);
	vs_color = mix(colorA, colorB, gl_VertexID % 2);
}