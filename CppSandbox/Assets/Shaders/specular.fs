#version 440 core
struct DirectionalLight
{
	vec3 direction;
	vec4 color;
	float intensity;
};

uniform struct Material
{
	vec3 ambient;
	vec3 albedo;
	vec3 specular;
	float specularExponent;
	float specularStrength;
} mat;

in vec3 out_worldPos;
in vec3 out_normal;
in vec2 out_uv;
in vec4 albedo;
in vec4 out_color;

out vec4 finalColor;

uniform DirectionalLight directionalLight;
uniform vec3 viewPos;

uniform sampler2D texAlbedo;

vec4 ComputeDirectional(DirectionalLight light, vec4 albedo, vec3 normal, Material mat)
{
	float specularStrength = mat.specularStrength;

	vec3 fragToEye = normalize(viewPos - out_worldPos);
	vec3 halfwayVector = normalize(fragToEye - normalize(light.direction));

	float specularIntensity = max(dot(normal, halfwayVector), 0.0);
	if (dot(normal, -light.direction) >= 0)
		specularIntensity = pow(specularIntensity, mat.specularExponent);
	else
		specularIntensity = 0;
	vec4 specular = vec4(specularIntensity * specularStrength * light.intensity * light.color.rgb * mat.specular, 1.0);

	vec4 diffuse = vec4(max(dot(-light.direction, normal), 0.0) * light.intensity * light.color.rgb, 1.0);

	return (diffuse + specular);
}

void main()
{
	vec4 textureColor = texture(texAlbedo, out_uv);
	vec4 mixTexAlbedo = vec4((mat.albedo.rgb * (1 - textureColor.a)) + (textureColor.rgb * textureColor.a), textureColor.a);

	vec3 fragToEye = normalize(viewPos - out_worldPos);
	vec3 halfwayVector = normalize(fragToEye - normalize(directionalLight.direction));
	finalColor = (out_color + ComputeDirectional(directionalLight, albedo, out_normal, mat)) * albedo * mixTexAlbedo;
}