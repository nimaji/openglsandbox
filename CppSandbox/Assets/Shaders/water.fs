#version 450

uniform int textureTiling;
uniform sampler2D heightMap;
uniform sampler2D albedo;
uniform vec3 viewLocation;
//layout(rgba8) restrict readonly uniform image2D gaussianNoiseTex;

uniform struct DirectionalLight
{
	vec3 direction;
	vec4 color;
	float intensity;
} directionalLight;

uniform struct AmbiantLight
{
	vec4 color;
	float intensity;
} ambiantLight;

uniform struct Material
{
	vec4 albedo;
	float roughness;
} mat;

uniform samplerCube cubemap;

out vec4 color;
in vec3 worldPos;
in vec2 fsNormCoords;
in vec3 normal;

#define M_PI 3.1415926535897932384626433832795

float Geometry(vec3 n, vec3 w, float k)
{
	float cosNH = max(dot(n, w), 0.0);
	return cosNH / (cosNH * (1.0 - k) + k);
}

float SmithGeometry(vec3 normal, vec3 toView, vec3 lightVec, float roughness)
{
	/*float h = roughness * roughness;*/
	float k = ((roughness + 1.0) * (roughness + 1.0)) / 8.0;

	//IBL version
	//float k = (roughness * roughness) / 2;
	return Geometry(normal, toView, k) * Geometry(normal, lightVec, k);
}


float NormalDistribution(vec3 normal, vec3 halfwayVector, float roughness)
{
	float sqR = roughness  * roughness * roughness * roughness;
	float dotNH = max(dot(normal, halfwayVector), 0.0f);
	float sqDotNH = dotNH * dotNH;
	float attenuation = sqDotNH * (roughness * roughness - 1.0f) + 1.0f;
	return sqR / (M_PI * attenuation * attenuation);
}

vec3 Fresnel(vec3 F0, vec3 halfwayVector, vec3 toView)
{
	return F0 + (1.0 - F0) * pow(1.0 - max(dot(halfwayVector, toView), 0.0), 5.0);
}

float chiGGX(float v)
{
    return v > 0 ? 1 : 0;
}

/*float GGX_Distribution(vec3 n, vec3 h, float alpha)
{
    float NoH = dot(n,h);
    float alpha2 = alpha * alpha * alpha * alpha;
    float NoH2 = NoH * NoH;
    float den = NoH2 * alpha2 + (1 - NoH2);
    return (chiGGX(NoH) * alpha2) / ( M_PI * den * den );
}*/

vec4 ComputeCookTorrance(DirectionalLight incommingLight, vec4 albedo, vec3 normal, Material mat)
{
	normal = normalize(normal);
	vec3 fragToEye = normalize(viewLocation - worldPos);
	vec3 halfwayVector = normalize(fragToEye + normalize(-incommingLight.direction));

	float roughness = mat.roughness;
	//float roughness = 1 - texture(texGlossiness, out_uv).r;
	float metallic = 0.001;

	vec3 lambert = albedo.rgb / M_PI;
	float N = NormalDistribution(normal, halfwayVector, roughness);
	float G = SmithGeometry(normal, fragToEye, -incommingLight.direction, roughness);

	vec3 F0 = vec3(0.04);
	F0 = mix(F0, albedo.rgb, metallic);
	vec3 F = Fresnel(F0, halfwayVector, fragToEye);
	//vec3 F = Fresnel(texture(texSpecular, out_uv).rgb, halfwayVector, fragToEye);
	
	vec3 cookTorrance = (N * G * F)
						/ (4.0 * max(dot(fragToEye, normal), 0.0) * max(dot(-incommingLight.direction, normal), 0.0) + 0.0001);

	vec3 ks = F;
	vec3 kd = (vec3(1.0) - ks) * (1.0 - metallic);
	vec3 finalRadiance = (kd * lambert + cookTorrance) * incommingLight.intensity * incommingLight.color.rgb * max(dot(-incommingLight.direction, normal), 0.0);
	//vec3 finalRadiance = lambert * incommingLight.intensity * incommingLight.color.rgb * max(dot(-incommingLight.direction, normal), 0.0);

	//return vec4(vec3(N), 1.0);
	return vec4(finalRadiance, 1.0);
}

float WaterFresnel(vec3 incommingLightDir, vec3 normal)
{
	float k = dot(incommingLightDir, normal);
	float g = 1.3333 + k * k - 1;
	float gmk = g-k;
	float gpk = g+k;
	float f = (k * gpk - 1);
	float fp = (k * gmk + 1);
	return gmk * gmk / (2 * gpk * gpk) * (1 + (f * f) / (fp * fp));
}

void main()
{
	//vec4 dirLightColor = directionalLight.color * max(dot(-directionalLight.direction, normal), 0) * directionalLight.intensity * 0.3;
	//vec4 dirLightColor = ComputeCookTorrance(directionalLight, mat.albedo, normal, mat);
	//vec4 ambiantColor = ambiantLight.color * ambiantLight.intensity;
	vec3 reflectedVector = reflect(normalize(worldPos - viewLocation), normal);
	vec4 ambiantColor = texture(cubemap, reflectedVector);
	
	vec4 fresnel = vec4(vec3(WaterFresnel(reflectedVector, normal)), 1);
	color = ambiantColor * fresnel + (1 - fresnel) * mat.albedo;/*dirLightColor + ambiantColor*/
	//color = fresnel;
	//color = vec4(normal, 1);
	//color = vec4(vec3(max(dot(-directionalLight.direction, normal), 0)), 1);
	//color = vec4(vec3(texture(heightMap, fsNormCoords).r), 1.0);
	//color = vec4(vec3(texture(albedo, fsNormCoords).r), 1.0 * textureTiling);
	//color = imageLoad(gaussianNoiseTex, ivec2(fsNormCoords* 512));
}