#version 440 core

layout(location = 0) in vec3 vertex;
layout(location = 1) in vec2 normal;

layout(location = 0) uniform mat4 viewProjection;

out vec3 out_vertex;
out vec3 out_texcoords;
out vec2 out_normal;

void main()
{
	gl_Position = viewProjection * vec4(vertex * 100, 1);
	out_normal = normal;
	out_texcoords = vertex;
}