#version 450

uniform sampler2D heightMap;
uniform mat4 model;
uniform mat4 viewProjection;

layout(location = 0) in vec2 normPosition;

void main()
{
	gl_Position = vec4(normPosition.x, /*texture(heightMap, normPosition.xy).r*/0, normPosition.y, 1);
	//gl_Position = vec4(normPosition.x, texture(heightMap, normPosition.xy).r, normPosition.y, 1);
}