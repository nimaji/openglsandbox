#version 440 core

layout(location = 0) in vec3 vertex;

layout(location = 0) uniform mat4 viewProjection;

void main()
{
	gl_Position = viewProjection * vec4(vertex, 1);
}