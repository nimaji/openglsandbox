#version 450

uniform mat4 M;
uniform mat4 VP;

in vec3 in_vertex;

void main()
{
	gl_Position = VP * M * vec4(in_vertex, 1);
}