#version 440 core

struct AmbiantLight
{
	vec4 color;
	float intensity;
};

uniform struct Material
{
	vec3 ambient;
	vec3 albedo;
	float metallic;
	float roughness;
} mat;

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normals;
layout (location = 2) in vec4 color;
layout (location = 3) in vec2 uv;
layout (location = 4) in vec3 tangent;

out vec3 out_worldPos;
out vec4 out_dirLightSpacePos;
out vec3 out_normal;
out vec2 out_uv;
out vec4 albedo;
out vec4 out_color;
out vec3 out_tangent;
out vec3 out_bitangent;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform mat4 normalMatrix;
uniform mat4 directionalViewProjection;

uniform AmbiantLight ambiantLight;

vec4 computeAmbiant(AmbiantLight light, vec3 matAmbiant)
{
	return vec4(light.color.xyz * light.intensity /** matAmbiant*/, 1.0);
}

void main()
{
	gl_Position = projection * view * model * vec4(position, 1.0);
	out_dirLightSpacePos = directionalViewProjection * model * vec4(position, 1.0);
	out_worldPos = (model * vec4(position, 1.0)).xyz;
	out_uv = uv;
	albedo = color;
	
	vec3 T = normalize(vec3(normalMatrix * vec4(tangent, 0.0)));
	vec3 N = normalize(vec3(normalMatrix * vec4(normals, 0.0)));
	// re-orthogonalize T with respect to N
	T = normalize(T - dot(T, N) * N);
	// then retrieve perpendicular vector B with the cross product of T and N
	vec3 B = cross(N, T);

	out_normal = N;
	out_tangent = T;
	out_bitangent = B;

	out_color = computeAmbiant(ambiantLight, mat.ambient);
	//out_color = vec4(tangent, 1);
}