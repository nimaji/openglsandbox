#version 440

const vec2 quadVert[6] = vec2[](
	vec2(-1.0, 1.0),
	vec2(-1.0, -1.0),
	vec2(1.0, -1.0),
	vec2(-1.0, 1.0),
	vec2(1.0, -1.0),
	vec2(1.0, 1.0)
);

const vec2 quadUV[6] = vec2[](
	vec2(0.0, 1.0),
	vec2(0.0, 0.0),
	vec2(1.0, 0.0),
	vec2(0.0, 1.0),
	vec2(1.0, 0.0),
	vec2(1.0, 1.0)
);

out vec2 out_uv;

void main()
{
	gl_Position = vec4(quadVert[gl_VertexID], 0.0, 1.0);
	out_uv = quadUV[gl_VertexID];
}