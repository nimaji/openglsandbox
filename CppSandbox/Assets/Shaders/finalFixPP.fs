#version 440

uniform sampler2D tex;

in vec2 out_uv;

out vec4 color;

uniform float exposure;

uniform vec4 fogColor;
uniform float fogDensity;
uniform sampler2D depthStencilMap;

uniform float cameraNear;
uniform float cameraFar;

#define M_PI 3.1415926535897932384626433832795

void main()
{
	color = texture(tex, out_uv);

	// Fog
	// Linearize depth
	float depth = texture(depthStencilMap, out_uv).r;
    float z_n = 2.0 * depth - 1.0;
    float z_e = 2.0 * cameraNear * cameraFar / (cameraFar + cameraNear - z_n * (cameraFar - cameraNear));
	// Apply fog
	if (depth < 1.0)
		color = mix(color, fogColor, min((fogDensity / 10000.0) * z_e, 1));
	
	// Exposure
	color = vec4(1) - exp(-color * exposure);
	//color = vec4(vec3(texture(depthStencilMap, out_uv).r), 1.0);
}