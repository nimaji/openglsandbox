mkdir %~n1Blurred
cd %1
for %%i in (jpg, tga, png, hdr) do (
	magick mogrify -path ../%~n1Blurred/ -blur 0x8 *.%%i
)
cd ../