#include "Rendering/Shader.hpp"

#include "Debug.hpp"

#ifdef _WIN32
#include "Rendering/OpenGL/OpenGLShader.hpp"
#elif _ORBIS
#include "Rendering/PS4/PS4Shader.hpp"
#else
Platform not supported
#endif

// Constructor generates the shader on the fly
void Shader::Initialize(const char* vertexPath, const char* fragmentPath, bool compileAuto)
{
	LoadShader(vertexPath, ShaderType::Vertex);
	LoadShader(fragmentPath, ShaderType::Fragment);
	if (compileAuto)
		Compile();
}

void Shader::Initialize(const char* computeShader)
{
	LoadShader(computeShader, ShaderType::Compute);
	Compile();
}

Shader* Shader::CreateShader(const char* vertexPath, const char* fragmentPath, bool compileAuto)
{
	auto shader = new PLATEFORM_SHADER();
	shader->Initialize(vertexPath, fragmentPath, compileAuto);
	return shader;
}

Shader* Shader::CreateShader(const char* computeShader)
{
	auto shader = new PLATEFORM_SHADER();
	shader->Initialize(computeShader);
	return shader;
}

void Shader::DestroyShader(Shader const* shader)
{
	if (shader)
		delete shader;
}