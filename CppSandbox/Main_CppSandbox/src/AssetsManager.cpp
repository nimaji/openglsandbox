#include "AssetsManager.hpp"

#include <iterator>
#include <sstream>
#include <string>
#include <algorithm>

#include "ConfigManager.hpp"
#include "Debug.hpp"
#include "IO/FileIO.hpp"

#include "Utils.hpp"
#include "AstImage.hpp"
#include "AstModel.hpp"
#include "AstShader.hpp"
AssetsManager::AssetsManager()
{
}

AssetsManager::~AssetsManager()
{
	for (auto&& asset : _assets)
		delete asset.second;
}

AssetsManager& AssetsManager::GetInstance()
{
	static AssetsManager manager;
	return manager;
}

void AssetsManager::Init()
{
	LoadAssets();
}

void AssetsManager::LoadAssets()
{
	AddRecursivelyAssetsFromPath(GetAssetsPath());
}	
std::string AssetsManager::GetAssetsPath()
{
	std::string assetsPath = CONFIG_MGR.GetAssetsPath();
	if (assetsPath == "")
	{
		ELog("Couldn't get the asset path form the configuration file.");
		//printf("ERROR: " "Couldn't get the asset path form the configuration file." "\n");
		return assetsPath;
	}
	else
	{
		Log("Assets loaded at path %s", assetsPath.c_str());
		return assetsPath;
	}
}

void AssetsManager::AddRecursivelyAssetsFromPath(std::string const& path)
{
	auto subFolders = FileIO::GetFoldersInDirectory(path.c_str());
	for (auto&& subFolder : subFolders)
		AddRecursivelyAssetsFromPath(path + subFolder + "/");

	auto files = FileIO::GetFilesInDirectory(path.c_str());
	for (auto&& file : files)
	{
		Asset::Type type = FindFileType(path + file);
		switch (type)
		{
		case Asset::Type::UNKNOWN:
			_assets[path + file] = new Asset(type, path + file);
			break;
		case Asset::Type::IMAGE:
			_assets[path + file] = new AstImage(path + file);
			break;
		case Asset::Type::SHADER:
			_assets[path + file] = new AstShader(path + file);
			break;
		case Asset::Type::MODEL:
			_assets[path + file] = new AstModel(path + file);
			break;
		default:
			break;
		}
	}
}

Asset::Type AssetsManager::FindFileType(std::string const& path)
{
	auto assetTypeExtentionsMap = CONFIG_MGR.GetAssetTypeExtentions();

	std::string searchedExtention = FileIO::GetExtention(path);
	for (auto&& pair : assetTypeExtentionsMap)
		for (auto&& typeExtention : pair.second)
			if (typeExtention == Utils::Strings::ToLower(searchedExtention))
				return Asset::StringToType(pair.first);

	return Asset::Type::UNKNOWN;
}