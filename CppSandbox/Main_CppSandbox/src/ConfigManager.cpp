#include "ConfigManager.hpp"

#include <string>
#include <sstream>
#include <iterator>

#include "Debug.hpp"
#include "IO/FileIO.hpp"

ConfigManager::ConfigManager()
{
}


ConfigManager& ConfigManager::GetInstance()
{
	static ConfigManager manager;
	return manager;
}

ConfigManager::~ConfigManager()
{
	if (_currentDocument)
		delete _currentDocument;
}

bool ConfigManager::LoadConfigFile(const char * filename)
{
	std::string strFName = FileIO::TransformToCurrentPlateform(filename);
	filename = strFName.c_str();
	TiXmlDocument* document = new TiXmlDocument(filename);
	bool loadOkay = document->LoadFile();
	if (loadOkay)
		Log("%s successfully loaded.", filename);
	else
		RELog(false, "Couldn't load the xml file %s.", filename);

	if (_currentDocument != nullptr)
		delete _currentDocument;

	_currentDocument = document;

	return true;
}

/// Return the string at AssetPath/Path of the config file.
/// If an error happend an empty string is returned.
std::string ConfigManager::GetAssetsPath() const
{
	auto rootElement = GetRootElement("AssetsPath");
	if (!rootElement)
		return "";

	std::string path = "";
	if (rootElement->QueryStringAttribute("Path", &path) != TIXML_SUCCESS)
		return "";
	return path;
}

/// Return the string at AssetPath/Path of the config file.
/// If an error happend an empty string is returned.
AssetTypeExtentionMap ConfigManager::GetAssetTypeExtentions() const
{
	AssetTypeExtentionMap res;
	auto rootElement = GetRootElement("TypeExtentions");
	if (!rootElement)
		return res;

	auto element = rootElement->FirstChildElement();
	while (element)
	{
		std::string strType;
		if (element->QueryStringAttribute("Type", &strType) != TIXML_SUCCESS)
			continue;

		std::string extentionList;
		if (element->QueryStringAttribute("Extentions", &extentionList) != TIXML_SUCCESS)
			continue;

		auto valueIt = res.find(strType);
		if (valueIt == res.end())
		{
			auto emplaceResult = res.emplace(std::make_pair(strType, std::vector<std::string>()));
			if (!emplaceResult.second)
			{
				ELog("An error happened while trying to add the %s asset type extentions. Can happened when the type is already defined.", strType.c_str());
				continue;
			}
			valueIt = emplaceResult.first;
		}
		
		std::istringstream stream(extentionList);
		valueIt->second.assign(
			std::istream_iterator<std::string>(stream),
			std::istream_iterator<std::string>());

		element = element->NextSiblingElement();
	}

	return res;
}

CubemapFolderList ConfigManager::GetAvailableCubemaps() const
{
	CubemapFolderList res;
	
	auto rootElement = GetRootElement("Cubemaps");
	if (!rootElement)
		return res;

	auto element = rootElement->FirstChildElement();
	while (element)
	{
		std::string path;
		if (element->QueryStringAttribute("Path", &path) != TIXML_SUCCESS)
			continue;

		if (strcmp(element->Value(), "File") == 0)
		{
			AddCubemapFolderIfValid(path, res);
		}
		else if (strcmp(element->Value(), "Folder") == 0)
		{
			auto folders = FileIO::GetFoldersInDirectory(path.c_str());
			for (auto folder : folders)
				AddCubemapFolderIfValid(path + folder, res);
		}
		else
		{
			ELog("Invalid XML Element in /Cubemap/.");
		}

		element = element->NextSiblingElement();
	}

	return res;
}

void ConfigManager::AddCubemapFolderIfValid(std::string const& path, CubemapFolderList& vector) const
{
	std::string neededFiles[] = { "r", "l", "f", "u", "b", "d" };

	auto files = FileIO::GetFilesInDirectory(path.c_str());
	std::string extension = "";
	for (auto&& file : files)
	{
		for (int i = 0; i < 6; i++)
		{
			if (neededFiles[i] != "" && file.length() > 2 && file[0] == neededFiles[i][0])
			{
				if (extension == "")
					extension = file.substr(1);

				if (file.substr(1) == extension)
					neededFiles[i] = "";
			}
		}
	}

	/// Dont add path to vector if not valid.
	if (extension == "")
		return;
	for (auto&& file : neededFiles)
		if (file != "")
			return;

	vector.push_back({ path, extension });
}

TiXmlElement* ConfigManager::GetRootElement(std::string const& name) const
{
	Assert(_currentDocument);
	Assert(_currentDocument->RootElement());
	return _currentDocument->RootElement()->FirstChildElement(name);
}
