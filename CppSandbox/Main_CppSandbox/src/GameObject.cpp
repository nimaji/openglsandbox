#include "GameObject.hpp"

#include <algorithm>

#include "Rendering/Shader.hpp"
#include "Rendering/Material.hpp"

#include "imgui.h"
#include "Game.hpp"

#include "RuntimeVariable.hpp"

GameObject::GameObject(std::string const& name, GameObject* parent)
:	Object(name),
	_parent(parent)
{
	if (parent)
		parent->_children.push_back(this);
	GAME.gameObjects.push_back(this);
	_debugAuto = false;

	Position.GetRV().DisplayPrecision = 1.f;
	_transform.OnTransformChanged.AddListener([this](Transform const& transform) { Position.GetRV().SetValue(transform.GetPosition(), true); });

	BaseMaterial.GetRV().HideInEditor = true;
}

GameObject::~GameObject()
{
	for (auto node : _children)
		delete node;

	GAME.gameObjects.erase(std::find(GAME.gameObjects.begin(), GAME.gameObjects.end(), this));
	if (BaseMaterial)
		delete BaseMaterial;
}

void GameObject::Initialize()
{
	Object::Initialize();
	_transform.SetPosition(Position);
	Position.GetRV().OnValueChanged.AddListener([this]() { _transform.SetPosition(Position); });
}

void GameObject::Update()
{
	Object::Update();
}

void GameObject::DebugUpdate()
{
	Object::DebugUpdate();

	if (BaseMaterial && ImGui::TreeNode("Material"))
	{
		BaseMaterial.GetValue()->DebugUpdate();
		ImGui::TreePop();
	}
}

std::string GameObject::GetFullName()
{
	if (_parent)
		return _parent->GetFullName() + "d" + _name;
	return "GameObject:" + _name;
}

void GameObject::_InternalDebugUpdate()
{
	if (ImGui::TreeNode(_name.c_str()))
	{
		DebugUpdate();

		for (auto child : _children)
			child->_InternalDebugUpdate();
		ImGui::TreePop();
	}
}

glm::mat4x4 GameObject::GetWorldTransformation()
{
	return _transform.GetTransformMatrix() * (_parent ? _parent->GetWorldTransformation() : glm::mat4());
}