#include "Transform.hpp"
#include <glm/glm.hpp>

Transform::Transform()
{
}


Transform::~Transform()
{
}


void Transform::SetPosition(glm::vec3 const& newPos)
{
	_transformMat[0][3] = newPos.x;
	_transformMat[1][3] = newPos.y;
	_transformMat[2][3] = newPos.z;
	OnTransformChanged.Invoke(*this);
}

glm::vec3 Transform::GetPosition() const
{
	return glm::vec3(_transformMat[0][3], _transformMat[1][3], _transformMat[2][3]);
}

glm::mat4 const& Transform::GetTransformMatrix() const
{
	return _transformMat;
}

void Transform::SetTransformMatrix(glm::mat4 const& mat)
{
	_transformMat = mat;
	OnTransformChanged.Invoke(*this);
}