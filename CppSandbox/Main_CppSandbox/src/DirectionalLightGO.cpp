#include "DirectionalLightGO.hpp"

#include "imgui.h"
#include "glm/gtx/transform.hpp"

#include "Rendering/Lights/ShadowMap.hpp"

DirectionalLightGO::DirectionalLightGO(std::string const& name)
: LightGO(name)
{
	_shadowMap = ShadowMap::CreateShadowMap();
	_persistentBoard->AddVal(RuntimeVariable::Allocate("Direction", glm::vec3(0, 0, 1)));
	_persistentBoard->AddVal(RuntimeVariable::Allocate("MakeRotate", false));
	_persistentBoard->AddVal(RuntimeVariable::Allocate("DrawShadows", true));

	ShadowBias.GetRV().DisplayPrecision = 0.001f;
	_shadowMap->SetBias(static_cast<float>(ShadowBias));
	ShadowBias.GetRV().OnValueChanged.AddListener([this]()
	{
		if (_shadowMap)
			_shadowMap->SetBias(static_cast<float>(ShadowBias));
	});
}


DirectionalLightGO::~DirectionalLightGO()
{
	if (_shadowMap)
		ShadowMap::DestroyShadowMap(_shadowMap);
}

void DirectionalLightGO::Update()
{
	LightGO::Update();
	if (_persistentBoard->QuickGetBoolVal("MakeRotate"))
		SetDirection(glm::rotate(0.05f, glm::vec3(0.f, 1.f, 0.f)) * glm::vec4(GetDirection(), 0));
}

void DirectionalLightGO::DebugUpdate()
{
	LightGO::DebugUpdate();
	if (ImGui::Button("Normalize Direction"))
		SetDirection(glm::normalize(GetDirection()));
}

glm::mat4 DirectionalLightGO::GetViewProjection() const
{
	auto proj = glm::ortho(-10.0f, 20.0f, -10.0f, 20.0f, 1.0f, 30.f);
	auto view = glm::lookAt(-15.f * GetDirection(), GetDirection(), { 0.f, 1.f, 0.f });
	return proj * view;
}
