#include "Game.hpp"

#include "Imgui.h"

#include "BoardPool.hpp"
#include "IO/Input.hpp"
#include "Rendering/RenderingContext.hpp"
#include "Editor/Editor.hpp"
#include "Tools/Time.hpp"

Game::Game()
{
}


Game::~Game()
{
}

void Game::Init()
{
	directional = new DirectionalLightGO("DefaultDirectional");
	ambiant = new LightGO("DefaultAmbiant");
	camera = new Camera("DefaultCamera");

	_startTime = Time::GetTimeInSec();
}

Game& Game::GetInstance()
{
	static Game game;
	return game;
}

void Game::UpdateView(Input& input)
{
	float deltaTime = static_cast<float>(GetDeltaTime());
	float cameraSpeed;
	if (input.SlowMoveAction.IsRequested())
		cameraSpeed = 5.0f * deltaTime;
	else if (input.FastMoveAction.IsRequested())
		cameraSpeed = 2000.0f * deltaTime;
	else
		cameraSpeed = 30.0f * deltaTime;

	camera->pos += input.MoveAxis.z * cameraSpeed * camera->front;
	camera->pos += input.MoveAxis.y * cameraSpeed * camera->up;
	camera->pos += input.MoveAxis.x * glm::normalize(glm::cross(camera->front, camera->up)) * cameraSpeed;

	camera->yaw += input.ViewAxis.x;
	camera->pitch += input.ViewAxis.y;

	// Make sure that when pitch is out of bounds, screen doesn't get flipped
	if (camera->pitch > 89.0f)
		camera->pitch = 89.0f;
	if (camera->pitch < -89.0f)
		camera->pitch = -89.0f;

	glm::vec3 front;
	front.x = cos(glm::radians(static_cast<float>(camera->yaw))) * static_cast<float>(cos(glm::radians(camera->pitch)));
	front.y = sin(glm::radians(static_cast<float>(camera->pitch)));
	front.z = sin(glm::radians(static_cast<float>(camera->yaw))) * static_cast<float>(cos(glm::radians(camera->pitch)));
	camera->front = glm::normalize(front);
}

void Game::Update()
{
	double currentFrame = GetTime();
	_deltaTime = currentFrame - _lastFrameTime;
	_lastFrameTime = currentFrame;

	Input* input = Input::GetCurrent();
	VRAssert(input);
	if (!EDITOR.IsEnabled())
		UpdateView(*input);
	if (input->SaveAction.WasTriggered())
		SaveDefault();

	for (auto go : objects)
		if (!go->IsInitialized())
			go->_InternalInit();

	for (auto obj : objects)
		obj->Update();

	resourceManager.UpdateThreadsStatus();
}

void Game::SaveDefault()
{
	boardPool.SaveDocument(_loadedSavePath);
}

void Game::LoadSave(std::string const & savePath)
{
	_loadedSavePath = savePath;
	boardPool.Load(savePath.c_str());
}

void Game::DebugUpdate()
{
	using namespace ImGui;

	if (Button(boardPool.IsDirty() ? "Save Scene *" : "Save Scene", ImVec2(100, 20)))
		boardPool.SaveDocument(_loadedSavePath);

	for (auto&& obj : objects)
	{
		if (obj->IsDebugAuto())
		{
			if (ImGui::TreeNode(obj->GetName().c_str()))
			{
				VRAssert(obj);
					obj->DebugUpdate();
				ImGui::TreePop();
			}
		}
	}

	GAME.rootGO->_InternalDebugUpdate();
}

double Game::GetTime() const 
{ 
	auto time = Time::GetTimeInSec();
	RAssert(_startTime != -1, time);
	return time - _startTime;
}
