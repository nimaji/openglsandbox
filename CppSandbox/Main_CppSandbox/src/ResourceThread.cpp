#include "ResourceThread.hpp"

#include "Debug.hpp"

std::mutex ResourceThread::canLoad;
std::atomic<int> ResourceThread::threadWorkingCount{ 0 };
std::condition_variable ResourceThread::conditionVariable;

ResourceThread::ResourceThread()
{
}

ResourceThread::~ResourceThread()
{
	if (thread)
	{
		Wait();
		delete thread;
	}
}

void ResourceThread::Wait()
{
	if (thread->joinable())
		thread->join();
}

bool ResourceThread::IsLoadSuccess()
{
	return Texture && result == AsyncLoadResult::DONE;
}

bool ResourceThread::IsLoadDone()
{
	return Texture && result != AsyncLoadResult::PENDING;
}

bool ResourceThread::InstallLoadedResource()
{
	if (!IsLoadSuccess())
		return false;
	RAssert(Texture, false);
	Texture->LoadPreparedImageAsync(TextureSizeXY[0], TextureSizeXY[1], LinearSpace);
	return true;
}
