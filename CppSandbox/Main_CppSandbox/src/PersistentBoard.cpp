#include "PersistentBoard.hpp"

#include <algorithm>

#include "imgui.h"
#include "tinyxml.h"
#include "Debug.hpp"
#include "Object.hpp"
#include "Game.hpp"
#include "Asset.hpp"

PersistentBoard::PersistentBoard()
{
}


PersistentBoard::~PersistentBoard()
{
	for(auto var : _vecRuntimeVars)
	{
		Assert(var);
		if (var)
			delete var;
	}
}

void PersistentBoard::SaveRuntimeVar(TiXmlElement* element, RuntimeVariable const& var) const
{
	VRAssert(element);
	element->SetAttribute("Type", static_cast<int>(var.GetType()));
	switch(var.GetType())
	{
		case RuntimeVariable::Type::DOUBLE:
			SaveRuntimeVarToXml<double>(element, var.GetDoubleValue());
			break;

		case RuntimeVariable::Type::STRING:
			SaveRuntimeVarToXml<std::string>(element, var.GetStringValue());
			break;

		case RuntimeVariable::Type::INT:
			SaveRuntimeVarToXml<int>(element, var.GetIntValue());
			break;

		case RuntimeVariable::Type::BOOL:
			SaveRuntimeVarToXml<bool>(element, var.GetBoolValue());
			break;

		case RuntimeVariable::Type::VEC3:
			SaveRuntimeVarToXml<glm::vec3>(element, var.GetVec3Value());
			break;

		case RuntimeVariable::Type::VEC4:
			SaveRuntimeVarToXml<glm::vec4>(element, var.GetVec4Value());
			break;

		case RuntimeVariable::Type::OBJECT:
			SaveRuntimeVarToXml<ObjectHandle>(element, var.GetObjectHandleValue());
			break;
		default:
			break;
	}
}

void PersistentBoard::Save(TiXmlElement* element)
{
	VRAssert(element);
	for (auto&& var : _vecRuntimeVars)
	{
		TiXmlElement* varElement = new TiXmlElement(var->GetName());
		SaveRuntimeVar(varElement, *var);
		element->LinkEndChild(varElement);
	}
	_isDirty = false;
}

void PersistentBoard::LoadRuntimeVar(TiXmlElement const* element)
{
	RuntimeVariable& value = *(new RuntimeVariable());
	value.SetName(element->ValueStr());
	int type;

	if (element->QueryIntAttribute("Type", &type) == TIXML_SUCCESS)
	{
		if (type >= 0 && type < static_cast<int>(RuntimeVariable::Type::UNKNOWN))
			value.SetType(static_cast<RuntimeVariable::Type>(type));
	}
	else
	{
		ELog("No type found while loading runtime var. Skipping");
		return;
	}

	bool result = false;
	switch (static_cast<RuntimeVariable::Type>(type))
	{
		case RuntimeVariable::Type::DOUBLE:
			result = SetRuntimeValFromElement<double>(element, value);
		break;
		case RuntimeVariable::Type::STRING:
			result = SetRuntimeValFromElement<std::string>(element, value);
			break;
		case RuntimeVariable::Type::INT:
			result = SetRuntimeValFromElement<int>(element, value);
			break;
		case RuntimeVariable::Type::BOOL:
			result = SetRuntimeValFromElement<bool>(element, value);
			break;
		case RuntimeVariable::Type::VEC3:
			result = SetRuntimeValFromElement<glm::vec3>(element, value);
			break;
		case RuntimeVariable::Type::VEC4:
			result = SetRuntimeValFromElement<glm::vec4>(element, value);
			break;
		case RuntimeVariable::Type::OBJECT:
			result = SetRuntimeValFromElement<ObjectHandle>(element, value);
			break;
		default:
			break;
	}

	if (!result)
	{
		ELog("Couldn't get the value of %s. Skipping.", value.GetName().c_str());
		return;
	}
	
	AddVal(&value);
}

void PersistentBoard::Load(TiXmlElement const* element)
{
	for (auto child = element->FirstChildElement(); child != nullptr; child = child->NextSiblingElement())
		LoadRuntimeVar(child->ToElement());
}

bool PersistentBoard::GetVal(std::string const& key, RuntimeVariable*& value) const
{
	auto res = _mapRuntimeVars.find(key);
	if (res != _mapRuntimeVars.end())
	{
		value = res->second;
		return true;
	}
	value = nullptr;
	return false;
}

bool PersistentBoard::AddVal(std::string const& key, RuntimeVariable* value)
{
	RAssert(_mapRuntimeVars.find(key) == _mapRuntimeVars.end(), false);
	_mapRuntimeVars.emplace(key, value);
	_vecRuntimeVars.push_back(value);
	return true;
}

RuntimeVariable* PersistentBoard::AddVal(RuntimeVariable* value)
{
	if (AddVal(value->GetName(), value))
		return value;
	else
		return nullptr;
}

void PersistentBoard::DebugUpdate()
{
	for (auto val : _vecRuntimeVars)
	{
		VRAssert(val);
		if (val->HideInEditor)
			continue;

		switch(val->GetType())
		{
			case RuntimeVariable::Type::STRING:
			{
				std::string const& sRes = val->GetStringValue();
				char csRes[255];
				strcpy_s(csRes, sRes.substr(0, 254).c_str());
				if (ImGui::InputText(val->GetName().c_str(), csRes, 255))
					val->SetValue(std::string(csRes));
			}
			break;
			case RuntimeVariable::Type::INT:
			{
				int iRes = val->GetIntValue();
				if (ImGui::InputInt(val->GetName().c_str(), &iRes))
					val->SetValue(iRes);
			}
			break;
			case RuntimeVariable::Type::DOUBLE:
			{
				float fRes = (float)val->GetDoubleValue();
				if (ImGui::DragFloat(val->GetName().c_str(), &fRes, val->DisplayPrecision, val->DisplayMin, val->DisplayMax))
					val->SetValue<double>(static_cast<double>(fRes));
			}
			break;
			case RuntimeVariable::Type::BOOL:
			{
				auto fRes = val->GetBoolValue();
				if (ImGui::Checkbox(val->GetName().c_str(), &fRes))
					val->SetValue(fRes);
			}
			break;
			case RuntimeVariable::Type::VEC3:
			{
				auto fRes = val->GetVec3Value();
				if (ImGui::DragFloat3(val->GetName().c_str(), &fRes[0], val->DisplayPrecision, val->DisplayMin, val->DisplayMax))
					val->SetValue(fRes);
			}
			break;
			case RuntimeVariable::Type::VEC4:
			{
				auto fRes = val->GetVec4Value();
				if (ImGui::DragFloat4(val->GetName().c_str(), &fRes[0], val->DisplayPrecision, val->DisplayMin, val->DisplayMax))
					val->SetValue(fRes);
			}
			break;
			case RuntimeVariable::Type::OBJECT:
			{
				auto fRes = val->GetObjectValue();

				static RuntimeVariable* objectSelectOpenProprietary = nullptr;
				ImGui::Text(val->GetName().c_str());
				ImGui::SameLine();
				ImGui::PushID(val);
				if (ImGui::Button(fRes ? fRes->GetName().c_str() : "........"))
					objectSelectOpenProprietary = val;
				ImGui::PopID();
				if (objectSelectOpenProprietary == val)
				{
					bool bOpen = true;
					if (ImGui::Begin("Select Object", &bOpen))
					{
						static char searchInput[512] = "";
						ImGui::InputText("Search", searchInput, 512);
						ImGui::BeginChild("##scrolling");
						if (ImGui::Button("None"))
							val->SetValue<Object*>(nullptr);

						std::string searchVal(searchInput);
						auto list = GAME.GetObjectsIf(val->DoInheritsFromType);
						for (auto&& item : list)
						{
							if (searchVal != "" && item->GetFullName().find(searchVal) == std::string::npos)
								continue;
							if (ImGui::Button(item->GetFullName().c_str()))
								val->SetValue<Object*>(item);
						}
						ImGui::EndChild();
					}
					if (!bOpen)
						objectSelectOpenProprietary = nullptr;
					ImGui::End();
				}
			}
			break;
			default:
				break;
		}
	}
}

void PersistentBoard::UpdateFrom(PersistentBoard const& from)
{
	for (auto val : _mapRuntimeVars)
	{
		auto res = from._mapRuntimeVars.find(val.first);
		if (res != from._mapRuntimeVars.end())
		{
			if (val.second->GetType() == res->second->GetType())
			{
				val.second->CopyDataFrom(*res->second);
				val.second->OnValueChanged.AddListener([this](){ this->Dirty(); });
			}
		}
	}
}

#define QuickGetValDeclare(type, type_nice_name) type PersistentBoard::QuickGet##type_nice_name##Val(std::string const & key) const\
{\
	RuntimeVariable* var = nullptr;\
	GetVal(key, var);\
	RAssert(var && "Couldn't get the asked value.", type());\
\
	return var->Get##type_nice_name##Value();\
}

QuickGetValDeclare(double, Double)
QuickGetValDeclare(int, Int)
QuickGetValDeclare(std::string, String)
QuickGetValDeclare(bool, Bool)
QuickGetValDeclare(glm::vec3, Vec3)
QuickGetValDeclare(glm::vec4, Vec4)

Object* PersistentBoard::QuickGetObjectVal(std::string const & key) const
{
	RuntimeVariable* var = nullptr;
	GetVal(key, var);
	RAssert(var && "Couldn't get the asked value.", nullptr);

	return var->GetObjectValue();
}

#undef QuickGetValDeclare

void PersistentBoard::Dirty()
{
	_isDirty = true;
	OnDirty.Invoke();
}


template<>
void PersistentBoard::SaveRuntimeVarToXml<double>(TiXmlElement* element, double const& value) const
{
	element->SetDoubleAttribute("Value", value);
}

template<>
void PersistentBoard::SaveRuntimeVarToXml<glm::vec3>(TiXmlElement* element, glm::vec3 const& value) const
{
	element->SetDoubleAttribute("x", value.x);
	element->SetDoubleAttribute("y", value.y);
	element->SetDoubleAttribute("z", value.z);
}

template<>
void PersistentBoard::SaveRuntimeVarToXml<glm::vec4>(TiXmlElement* element, glm::vec4 const& value) const
{
	element->SetDoubleAttribute("x", value.x);
	element->SetDoubleAttribute("y", value.y);
	element->SetDoubleAttribute("z", value.z);
	element->SetDoubleAttribute("w", value.w);
}

template<>
void PersistentBoard::SaveRuntimeVarToXml<ObjectHandle>(TiXmlElement* element, ObjectHandle const& value) const
{
	element->SetAttribute("ID", value.GetSerializableValue());
}


template<>
bool PersistentBoard::SetRuntimeValFromElement<std::string>(TiXmlElement const* element, RuntimeVariable& rVar) const
{
	RAssert(element, false);
	std::string val;
	auto res = element->QueryStringAttribute("Value", &val);
	if (res == TIXML_SUCCESS)
	{
		rVar.SetValue(val, true);
		return true;
	}

	return false;
}

template<>
bool PersistentBoard::SetRuntimeValFromElement<glm::vec3>(TiXmlElement const* element, RuntimeVariable& rVar) const
{
	glm::vec3 res;
	if (element->QueryValueAttribute<float>("x", &res.x) != TIXML_SUCCESS)
		return false;
	if (element->QueryValueAttribute<float>("y", &res.y) != TIXML_SUCCESS)
		return false;
	if (element->QueryValueAttribute<float>("z", &res.z) != TIXML_SUCCESS)
		return false;

	rVar.SetValue(res, true);

	return true;
}

template<>
bool PersistentBoard::SetRuntimeValFromElement<glm::vec4>(TiXmlElement const* element, RuntimeVariable& rVar) const
{
	glm::vec4 res;
	if (element->QueryValueAttribute<float>("x", &res.x) != TIXML_SUCCESS)
		return false;
	if (element->QueryValueAttribute<float>("y", &res.y) != TIXML_SUCCESS)
		return false;
	if (element->QueryValueAttribute<float>("z", &res.z) != TIXML_SUCCESS)
		return false;
	if (element->QueryValueAttribute<float>("w", &res.w) != TIXML_SUCCESS)
		return false;

	rVar.SetValue(res, true);

	return true;
}

template<>
bool PersistentBoard::SetRuntimeValFromElement<ObjectHandle>(TiXmlElement const* element, RuntimeVariable& rVar) const
{
	std::string serializedVal;
	if (element->QueryStringAttribute("ID", &serializedVal) != TIXML_SUCCESS)
		return false;

	rVar.SetValue(ObjectHandle(serializedVal), true);

	return true;
}