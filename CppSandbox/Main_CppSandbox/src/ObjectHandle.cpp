#include "ObjectHandle.hpp"

#include "Object.hpp"
#include "Game.hpp"

ObjectHandle::ObjectHandle(Object* obj)
: _obj(obj)
{
	_obj = obj;
	if (obj)
		_name = obj->GetFullName();
}

ObjectHandle::ObjectHandle(std::string const& serializableValue)
:	_name(serializableValue)
{
	_obj = RetrieveObject();
}

ObjectHandle::~ObjectHandle()
{
}

std::string ObjectHandle::GetSerializableValue() const
{
	return _name;
}

Object* ObjectHandle::RetrieveObject()
{
	return GAME.GetObjectOfFullName(_name);
}