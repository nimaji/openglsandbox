#include "Object.hpp"

#include "PersistentBoard.hpp"
#include "Game.hpp"

Object::Object(std::string const& name)
: _name(name)
{
	_persistentBoard = new PersistentBoard();
	GAME.objects.push_back(this);
}

Object::~Object()
{
	if (_persistentBoard)
		delete _persistentBoard;
}

void Object::_InternalInit()
{
	GAME.boardPool.UpdatePersistantBoard(GetFullName(), _persistentBoard, true);
	Initialize();
	_initialized = true;
}

void Object::DebugUpdate()
{
	if (_persistentBoard)
		_persistentBoard->DebugUpdate();
}
