#include "NormalizedDouble.h"

NormalizedDouble::NormalizedDouble(std::string const& name, double value)
{
	SetName(name);
	SetType(RuntimeVariable::Type::DOUBLE);
	SetValue(value, true);
	DisplayMax = 1.f;
	DisplayMin = 0.f;
	DisplayPrecision = 0.001f;
}


NormalizedDouble::~NormalizedDouble()
{
}
