#include "IO/Windows/GLFWInput.hpp"

#include "GLFW/glfw3.h"
#include "Rendering/OpenGL/OpenGLRenderingContext.hpp"
#include "Debug.hpp"
#include "System.hpp"
#include "Editor/Editor.hpp"

#include "imgui.h"
#include "imgui_impl_glfw_gl3.h"

GLFWInput::GLFWInput()
{
}

GLFWInput::~GLFWInput()
{
}

void GLFWInput::Initialize()
{
	if (GetCurrent() != nullptr)
	{
		ELog("Input already settup, please shutdown it before initializing a new input object.");
		return;
	}

	GLFWwindow* window = OpenGLRenderingContext::sGetGLFWWindow();
	Assert(window);

	// Set the required callback functions
	glfwSetKeyCallback(window, KeyCallback);
	glfwSetCursorPosCallback(window, MouseCallback);
	glfwSetScrollCallback(window, ScrollCallback);
	glfwSetCharCallback(window, CharCallback);
	glfwSetMouseButtonCallback(window, MouseButtonCallback);

	glfwGetCursorPos(window, &CursorPos.x, &CursorPos.y);

	SetCurrent(this);
}

void GLFWInput::Shutdown()
{
	RAssert(TestValidity());

	GLFWwindow* window = OpenGLRenderingContext::sGetGLFWWindow();
	Assert(window);

	// Set the required callback functions
	glfwSetKeyCallback(window, nullptr);
	glfwSetCursorPosCallback(window, nullptr);
	glfwSetScrollCallback(window, nullptr);
	glfwSetCharCallback(window, nullptr);
	glfwSetMouseButtonCallback(window, nullptr);
	SetCurrent(nullptr);
}

void GLFWInput::Update()
{
	RAssert(TestValidity());

	for (int i = 0; i < _actionsCount; i++)
		_actions[i]->FrameClear();
	CursorMove = glm::dvec2(0, 0);
	ViewAxis = CursorMove;
	// Check if any events have been activiated (key pressed, mouse moved etc.) and call corresponding response functions
	glfwPollEvents();

	if (glfwWindowShouldClose(OpenGLRenderingContext::sGetGLFWWindow()))
		SYSTEM.AskCloseApplication();
	MoveAxis = glm::vec3();
	if (_moveInputs[0])
		MoveAxis.x -= 1.f;
	if (_moveInputs[1])
		MoveAxis.x += 1.f;
	if (_moveInputs[2])
		MoveAxis.y += 1.f;
	if (_moveInputs[3])
		MoveAxis.y -= 1.f;
	if (_moveInputs[4])
		MoveAxis.z += 1.f;
	if (_moveInputs[5])
		MoveAxis.z -= 1.f;
}

void GLFWInput::DoHideCursor(bool doHideCursor)
{
	OpenGLRenderingContext* context = dynamic_cast<OpenGLRenderingContext*>(RenderingContext::sGetContext());
	Assert(context);

	if (doHideCursor)
		glfwSetInputMode(context->GetGLFWWindow(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	else
	{
		glfwSetInputMode(context->GetGLFWWindow(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		CursorPos = glm::dvec2(RenderingContext::sGetWidth() / 2.0, RenderingContext::sGetHeight() / 2.0);
		glfwSetCursorPos(context->GetGLFWWindow(), RenderingContext::sGetWidth() / 2.0, RenderingContext::sGetHeight() / 2.0);
	}
}

void GLFWInput::UpdateKey(InputAction& action, int key, int bindedKey, int keyAction)
{
	if (key == bindedKey)
	{
		if (keyAction == GLFW_PRESS)
			action.SetTriggered();
		else if (keyAction == GLFW_RELEASE)
			action.SetUntriggered();
	}
}

// Is called whenever a key is pressed/released via GLFW
void GLFWInput::KeyCallback(GLFWwindow* window, int key, int scancode, int action, int _mods)
{
	GLFWInput* input = dynamic_cast<GLFWInput*>(GLFWInput::GetCurrent());
	RAssert(input);

	input->UpdateKey(input->CloseAction, key, GLFW_KEY_ESCAPE, action);
	input->UpdateKey(input->DebugAction, key, GLFW_KEY_GRAVE_ACCENT, action);
	if (_mods == GLFW_MOD_CONTROL)
		input->UpdateKey(input->SaveAction, key, GLFW_KEY_S, action);
	input->UpdateKey(input->FastMoveAction, key, GLFW_KEY_SPACE, action);
	input->UpdateKey(input->SlowMoveAction, key, GLFW_KEY_LEFT_SHIFT, action);

	if (EDITOR.IsEnabled())
		ImGui_ImplGlfwGL3_KeyCallback(window, key, scancode, action, _mods);

	bool isPressed = action == GLFW_PRESS;

	if (isPressed || (!isPressed && action == GLFW_RELEASE))
	{
		if (key == GLFW_KEY_A)
			input->_moveInputs[0] = isPressed;
		else if (key == GLFW_KEY_D)
			input->_moveInputs[1] = isPressed;
		else if (key == GLFW_KEY_Q)
			input->_moveInputs[2] = isPressed;
		else if (key == GLFW_KEY_E)
			input->_moveInputs[3] = isPressed;
		else if (key == GLFW_KEY_W)
			input->_moveInputs[4] = isPressed;
		else if (key == GLFW_KEY_S)
			input->_moveInputs[5] = isPressed;
	}
}

void GLFWInput::MouseCallback(GLFWwindow* window, double xpos, double ypos)
{
	GLFWInput* input = dynamic_cast<GLFWInput*>(GLFWInput::GetCurrent());
	RAssert(input);

	glm::dvec2 pos(xpos, ypos);
	input->CursorMove =  pos - input->CursorPos;
	input->CursorPos = pos;

	input->ViewAxis = glm::dvec2(1.0, -1.0) * input->CursorMove * input->_mouseSensitivity;
}

void GLFWInput::ScrollCallback(GLFWwindow* window, double xoffset, double yoffset)
{
	GLFWInput* input = dynamic_cast<GLFWInput*>(GLFWInput::GetCurrent());
	RAssert(input);

	if (EDITOR.IsEnabled())
		ImGui_ImplGlfwGL3_ScrollCallback(window, xoffset, yoffset);
}

void GLFWInput::MouseButtonCallback(GLFWwindow* window, int button, int action, int mods)
{
	GLFWInput* input = dynamic_cast<GLFWInput*>(GLFWInput::GetCurrent());
	RAssert(input);

	if (EDITOR.IsEnabled())
		ImGui_ImplGlfwGL3_MouseButtonCallback(window, button, action, mods);
}

void GLFWInput::CharCallback(GLFWwindow* window, unsigned int c)
{
	GLFWInput* input = dynamic_cast<GLFWInput*>(GLFWInput::GetCurrent());
	RAssert(input);

	if (EDITOR.IsEnabled())
		ImGui_ImplGlfwGL3_CharCallback(window, c);
}

