#include "IO/Windows/WinFileIO.hpp"

#include "dirent.h"

namespace WinFileIO
{
std::vector<std::string> GetFoldersInDirectory(const char* path)
{
	std::vector<std::string> res;

	DIR* dir = opendir(path);
	if (!dir)
		return res;
	dirent* entry = readdir(dir);

	while (entry != NULL)
	{
		if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
		{
			entry = readdir(dir);
			continue;
		}
		if (entry->d_type == DT_DIR)
			res.emplace_back(entry->d_name);

		entry = readdir(dir);
	}

	closedir(dir);

	return res;
}

std::vector<std::string> GetFilesInDirectory(const char* path)
{
	std::vector<std::string> res;

	DIR* dir = opendir(path);
	if (!dir)
		return res;
	dirent* entry = readdir(dir);

	while (entry != NULL)
	{
		if (entry->d_type == DT_REG)
			res.emplace_back(entry->d_name);

		entry = readdir(dir);
	}

	closedir(dir);

	return res;
}

bool WinFileIO::FileExists(const char* path)
{
	struct stat buffer;
	return (stat(path, &buffer) == 0);
}

} // namespace WinFileIO