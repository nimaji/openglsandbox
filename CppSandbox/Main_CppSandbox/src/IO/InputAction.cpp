#include "IO/InputAction.hpp"

void InputAction::SetTriggered()
{
	_isTriggered = true;
	_isRequested = true;
}

void InputAction::SetUntriggered()
{
	_isUntriggered = true;
	_isRequested = false;
}

void InputAction::FrameClear()
{
	_isTriggered = false;
	_isUntriggered = false;
}

void InputAction::Clear()
{
	_isRequested = false;
	FrameClear();
}