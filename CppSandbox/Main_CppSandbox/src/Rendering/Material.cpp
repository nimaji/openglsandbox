#include "Rendering\Material.hpp"

#include "GameObject.hpp"

Material::Material(std::string const& name, GameObject* target)
:	Object(name),
	_targetGO(target)
{
	Initialize();
	_debugAuto = false;
}


Material::~Material()
{
	for (int i = 0; i < kMaxPass; i++)
		if (_shaders[i])
			delete _shaders[i];
	if (_shadowShader)
		delete _shadowShader;
}

void Material::SetShader(Shader* shader, int index)
{
	VRAssert(index <= kMaxPass);
	_shaders[index] = shader;
}

bool Material::IsValid()
{
	return _targetGO;
}