#include "Rendering/Lights/ShadowMap.hpp"

#ifdef _WIN32
#include "Rendering/Lights/OpenGL/OpenGLShadowMap.hpp"
#elif _ORBIS
#include "Rendering/Lights/PS4/PS4ShadowMap.hpp"
#else
Platform not supported
#endif

ShadowMap::ShadowMap()
{
}

ShadowMap::~ShadowMap()
{
}

ShadowMap* ShadowMap::CreateShadowMap()
{
#ifdef _WIN32
	return new OpenGLShadowMap();
#elif _ORBIS
	return new PS4ShadowMap();
#else
	Platform not supported
#endif
	return nullptr;
}

void ShadowMap::DestroyShadowMap(ShadowMap* shadowMap)
{
	delete shadowMap;
}