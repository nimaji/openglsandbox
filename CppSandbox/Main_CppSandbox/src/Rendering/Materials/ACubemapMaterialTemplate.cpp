#include "Rendering/Materials/ACubemapMaterialTemplate.hpp"

#include <algorithm>

#include "imgui.h"

#include "Game.hpp"
#include "ResourceManager.hpp"
#include "ConfigManager.hpp"

#include "Rendering/Shader.hpp"

ACubemapMaterialTemplate::ACubemapMaterialTemplate(std::string const& name, GameObject* attached)
:	Material(name, attached)
{
	LoadAvailableMaps();

	auto desc = Shader::CreateShader("Assets/Shaders/cubemap.vs", "Assets/Shaders/cubemap.fs");
	SetShader(desc, 0);
}


ACubemapMaterialTemplate::~ACubemapMaterialTemplate()
{
}

void ACubemapMaterialTemplate::LoadAvailableMaps()
{
	_availableMaps = CONFIG_MGR.GetAvailableCubemaps();
}

void ACubemapMaterialTemplate::Initialize()
{
	Material::Initialize();
	LoadCurrentMap();
}

void ACubemapMaterialTemplate::LoadCurrentMap()
{
	if (CurrentMap.GetValue() != "")
		LoadCubemap(CurrentMap);
}

bool ACubemapMaterialTemplate::LoadCubemap(std::string const& folderPath)
{
	_cubemapTex = GAME.resourceManager.LoadCubemap(folderPath);
	return _cubemapTex;
}

void ACubemapMaterialTemplate::DebugUpdate()
{
	using namespace ImGui;
	if (_availableMaps.empty())
		ImGui::Text("No maps available.");
	else
	{
		int count = 0;
		for (auto map : _availableMaps)
			count += map.first.size() + 1;
		char* charMap = new char[count + 1];
		int index = 0;
		for (auto map : _availableMaps)
		{
			int charCount = map.first.size() + 1;
			strcpy_s(charMap + index, charCount, map.first.c_str());
			index += charCount;
		}
		charMap[count] = '\0';

		std::string const& persistentFolder = CurrentMap.GetValue();
		int currentValue = GetCubemapIndexFromAvailable(persistentFolder);
		if (currentValue == -1)
			currentValue = 0;

		if (ImGui::Combo("Current map", &currentValue, charMap))
		{
			CurrentMap = _availableMaps[currentValue].first;
			LoadCurrentMap();
		}
		delete[] charMap;
	}
}

int ACubemapMaterialTemplate::GetCubemapIndexFromAvailable(std::string const& folderPath) const
{
	auto size = _availableMaps.size();
	for (int i = 0; i < size; i++)
	{
		if (_availableMaps[i].first == folderPath)
			return i;
	}
	return -1;
}
