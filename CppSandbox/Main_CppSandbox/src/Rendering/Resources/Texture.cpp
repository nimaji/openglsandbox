#include "Rendering/Resources/Texture.hpp"

bool Texture::SafeLoadImage(std::string const& path, bool linearSpace, bool isHDR)
{
	if (!LoadImage(path, linearSpace, isHDR))
		return LoadNullImage(2, 2);
	return true;
}

bool Texture::LoadNullImage(int sizeX, int sizeY, bool isHDR)
{
	if (!sizeX || !sizeY)
		return false;

	void* data = nullptr;
	if (!isHDR)
		data = new unsigned char[sizeX * sizeY * 4 * sizeof(float)]{ 0 };
	else
		data = new float[sizeX * sizeY * 4 * sizeof(float)];

	bool res = LoadImage(data, sizeX, sizeY, true, isHDR);
	delete[] data;
	return res;
}

bool Texture::IsLoadAsyncReady()
{
	return !_asyncLoadingID != -1;
}