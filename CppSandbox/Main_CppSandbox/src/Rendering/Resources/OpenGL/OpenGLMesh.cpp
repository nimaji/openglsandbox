#include "Rendering/Resources/OpenGL/OpenGLMesh.hpp"

#include "Debug.hpp"

OpenGLMesh::OpenGLMesh()
{
	glGenBuffers(1, &_vbo);
	glGenBuffers(1, &_ibo);
}

void OpenGLMesh::LoadData(Vertex* data, size_t count)
{
	VRAssert(data && count);

	/// VBO
	glBindBuffer(GL_ARRAY_BUFFER, _vbo);
	glBufferData(GL_ARRAY_BUFFER, count * sizeof(Vertex), data, GL_STATIC_DRAW);

	SetVerticesCount(count);
}

void OpenGLMesh::LoadIndices(uint32_t* indexes, size_t count)
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * sizeof(uint32_t), indexes, GL_STATIC_DRAW);

	SetIndicesCount(count);
}

OpenGLMesh::~OpenGLMesh()
{
	glDeleteBuffers(1, &_vbo);
	glDeleteBuffers(1, &_ibo);
	SetVerticesCount(0);
	SetIndicesCount(0);
	_vbo = 0;
	_ibo = 0;
}
