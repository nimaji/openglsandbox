#include "Rendering/Resources/OpenGL/OpenGLTexture.hpp"

#ifndef STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_IMPLEMENTATION
#endif
#include "stb_image.h"

#include <chrono>
#include <algorithm>
#include <thread>

#include "Debug.hpp"
#include "IO/FileIO.hpp"

OpenGLTexture::OpenGLTexture()
{
	glGenTextures(1, &_texture);
}

bool OpenGLTexture::LoadImage(std::string const& path, bool linearSpace, bool isHDR)
{
	if (!FileIO::FileExists(path.c_str()))
		return false;

	int channels;
	void* data = nullptr;

	int width = 0;
	int height = 0;

	if (IsHDR())
		data = stbi_loadf(path.c_str(), &width, &height, &channels, STBI_rgb);
	else
		data = stbi_load(path.c_str(), &width, &height, &channels, STBI_rgb);

	if (!data)
		return false;

	bool res = LoadImage(data, width, height, linearSpace, isHDR);

	stbi_image_free(data);
	return res;
}

bool OpenGLTexture::LoadImage(void* data, int width, int height, bool linearSpace, bool isHDR)
{
	if (!data)
		return false;

	_width = width;
	_height = height;
	SetIsHDR(isHDR);

	glBindTexture(GL_TEXTURE_2D, _texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	GLfloat maxAnisotropy;
	glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &maxAnisotropy);

	if (GLEW_EXT_texture_filter_anisotropic)
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, maxAnisotropy);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	if (IsHDR())
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, _width, _height, 0, GL_RGB, GL_FLOAT, data);
	else
		glTexImage2D(GL_TEXTURE_2D, 0, linearSpace ? GL_RGBA : GL_SRGB_ALPHA, _width, _height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
	glGenerateMipmap(GL_TEXTURE_2D);

	RAssert(!glGetError(), false);

	return true;
}

bool	OpenGLTexture::LoadCubemap(std::string const& path)
{
	unsigned char* imgDatas[6];
	glBindTexture(GL_TEXTURE_CUBE_MAP, _texture);

	std::string names[6] = { "r", "l", "u", "d", "b", "f" };
	auto filesList = FileIO::GetFilesInDirectory(path.c_str());
	std::string extension = "";
	for (auto file : filesList)
	{
		if (file.find(names[0] + ".") == 0)
			extension = FileIO::GetExtention(file);
	}

	if (extension == "")
		return false;

	int comp;
	for (int i = 0; i < 6; i++)
	{
		int textureSizes[2];
		auto imgData = stbi_loadf((path + "/" + names[i] + "." + extension).c_str(), textureSizes, textureSizes + 1, &comp, STBI_rgb);
		if (!imgData)
		{
			glDeleteTextures(1, &_texture);
			return false;
		}
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB16F, textureSizes[0], textureSizes[1], 0, GL_RGB, GL_FLOAT, imgData);
		stbi_image_free(imgData);
	}

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	for (GLenum e : { GL_TEXTURE_WRAP_S, GL_TEXTURE_WRAP_R, GL_TEXTURE_WRAP_T })
		glTexParameteri(GL_TEXTURE_CUBE_MAP, e, GL_CLAMP_TO_EDGE);

	return _texture;
}

AsyncLoadResult OpenGLTexture::PrepareImageAsync(std::string const& path, int* textureXY)
{
	if (!FileIO::FileExists(path.c_str()))
		return AsyncLoadResult::CANCELED;

	int loadID = ++_asyncLoadingID;

	unsigned char* res = nullptr;
	for (size_t i = 0; res == nullptr && i < _retryFileLoadingCount; i++)
	{
		res = stbi_load(path.c_str(), textureXY, textureXY + 1, nullptr, STBI_rgb_alpha);

		using namespace std::chrono_literals;
		std::this_thread::sleep_for(0.1s);
	}
	if (!res)
		return AsyncLoadResult::CANCELED;

	if (loadID != _asyncLoadingID)
		return AsyncLoadResult::CANCELED;

	_asyncLoadRes = res;
	_asyncLoadingID = -1;
	return AsyncLoadResult::DONE;
}

bool	OpenGLTexture::LoadPreparedImageAsync(int width, int height, bool linearSpace)
{
	if (_asyncLoadingID != -1)
		return false;

	bool res = LoadImage(_asyncLoadRes, width, height, linearSpace);

	stbi_image_free(_asyncLoadRes);
	_asyncLoadRes = nullptr;
	return res;
}

void OpenGLTexture::DropLoadAsync()
{
	if (_asyncLoadRes)
		stbi_image_free(_asyncLoadRes);
	_asyncLoadRes = nullptr;
	_asyncLoadingID != -1;
}
