#include "Rendering/OpenGL/OpenGLRenderingContext.hpp"

// GLEW
#define GLEW_STATIC
#include <GL/glew.h>

// GLFW
#include <GLFW/glfw3.h>
#include "GL/wglew.h"
#include "Debug.hpp"

OpenGLRenderingContext::OpenGLRenderingContext()
{
}

OpenGLRenderingContext::~OpenGLRenderingContext()
{
}

void OpenGLRenderingContext::ErrorHandle(int error, const char* description)
{
	ELog("GLFW:%i:%s", error, description);
}

void OpenGLRenderingContext::Initialize(RenderingContextDesc const& desc)
{
	// Init GLFW
	glfwInit();
	glfwSetErrorCallback(ErrorHandle);

	// Set all the required options for GLFW
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
	// Create a GLFWwindow object that we can use for GLFW's functions
	_window = glfwCreateWindow(static_cast<int>(desc.Width), static_cast<int>(desc.Height), desc.Name.c_str(), nullptr, nullptr);
	glfwMakeContextCurrent(_window);

	// Set this to true so GLEW knows to use a modern approach to retrieving function pointers and extensions
	glewExperimental = GL_TRUE;
	// Initialize GLEW to setup the OpenGL Function pointers
	glewInit();
	glViewport(0, 0, desc.Width, desc.Height);
	if (WGLEW_EXT_swap_control)
		wglSwapIntervalEXT(0);

	_initialized = true;
	_width = desc.Width;
	_height = desc.Height;
}

void OpenGLRenderingContext::Shutdown()
{
	glfwTerminate();
}

GLFWwindow* OpenGLRenderingContext::sGetGLFWWindow()
{
	OpenGLRenderingContext* context = dynamic_cast<OpenGLRenderingContext*>(sGetContext());
	Assert(context);
	return context->_window;
}