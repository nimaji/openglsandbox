#include "Water.hpp"

#include "Terrain.hpp"

#include "glm/glm.hpp"
#include <glm/gtc/type_ptr.hpp>

#include "Rendering/Shader.hpp"
#include "Game.hpp"
#include "AstImage.hpp"

#include "TessellatedPlane.hpp"

#include "complex.h"
#include "fft.h"

#include "Tools/OpenGL/OpenGLUtils.hpp"

#include "Random.hpp"

Water::Water(std::string const& name, GameObject* parent)
	: GameObject(name, parent)
{
	OriginalTesselationLevel.GetRV().OnValueChanged.AddListener(
		[this]()
	{
		if (_mesh)
			static_cast<TessellatedPlane*>(_mesh)->UpdateMesh(OriginalTesselationLevel);
	});

	HeightMapSize.GetRV().OnValueChanged.AddListener(
		[this]()
	{
		PrepareWaveData();
	});

	WindForce.GetRV().OnValueChanged.AddListener(
		[this]()
	{
		PrepareWaveData();
	});

	WaveHeightFactor.GetRV().OnValueChanged.AddListener(
		[this]()
	{
		PrepareWaveData();
	});
}

Water::~Water()
{
	if (_mesh)
		delete _mesh;
	if (_computeBaseWaveShader)
		delete _computeBaseWaveShader;
	if (_computeHeightMapShader)
		delete _computeHeightMapShader;

	//glDeleteBuffers(1, &_frequencyDomainBuffer);
	glDeleteBuffers(1, &_baseFrequencyBuffer);
	_baseFrequencyData = nullptr;

	GLuint texs[] = { /*_phaseAmplitudeMap, _oppositePhaseAmplitudeMap, */_heightMap };
	glDeleteTextures(1, texs);
}

void Water::Initialize()
{
	GameObject::Initialize();
	GenWavesData();
	PrepareWaveData();
	LoadRenderer();
}

void Water::PrepareWaveData()
{
	//glBindBuffer(GL_SHADER_STORAGE_BUFFER, _frequencyDomainBuffer);
	//glBufferData(GL_SHADER_STORAGE_BUFFER, HeightMapSize * HeightMapSize * sizeof(GLfloat) * 2, nullptr, GL_DYNAMIC_READ);
	//glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

	glBindBuffer(GL_SHADER_STORAGE_BUFFER, _baseFrequencyBuffer);
	glBufferData(GL_SHADER_STORAGE_BUFFER, HeightMapSize * HeightMapSize * sizeof(GLfloat) * 4, nullptr, GL_DYNAMIC_READ);
	if (_baseFrequencyData)
		_baseFrequencyData = nullptr;
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
	
	glBindTexture(GL_TEXTURE_2D, _gaussianNoiseTexture);
	float* values = new float[HeightMapSize * HeightMapSize * 4];
	RANDOM.GaussianNoiseArray(values, HeightMapSize * HeightMapSize * 4, 0, 1);
	Assert(values);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, HeightMapSize, HeightMapSize, 0, GL_RGBA, GL_FLOAT, values);
	glBindTexture(GL_TEXTURE_2D, 0);

	//auto textures = { _phaseAmplitudeMap, _oppositePhaseAmplitudeMap };
	//for (auto&& texture : textures)
	//{
	//	glBindTexture(GL_TEXTURE_2D, texture);
	//	glTexImage2D(GL_TEXTURE_2D, 0, GL_RG32F, HeightMapSize, HeightMapSize, 0, GL_RG, GL_FLOAT, nullptr);
	//}
	//glBindTexture(GL_TEXTURE_2D, 0);
	
	//ComputeBasePhaseAmplitudes(_phaseAmplitudeMap, _oppositePhaseAmplitudeMap);
	ComputeBasePhaseAmplitudes(0, 0);
}

void Water::Update()
{
	GameObject::Update();
	ComputeHeightMap(GAME.GetFrameTime(), _heightMap);
}

void Water::GenWavesData()
{
	_computeBaseWaveShader = new Shader("Assets/Shaders/waterBaseWaves.csh");
	_computeHeightMapShader = new Shader("Assets/Shaders/waterHeightMap.csh");

	auto SetupParams = []()
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	};

	//_gaussianNoiseTexture = GAME.resourceManager.LoadTexture("Assets/Textures/Noise/gaussian_noise.jpg", true);
	//_gaussianNoiseTexture = GAME.resourceManager.LoadTexture();
	glGenTextures(1, &_gaussianNoiseTexture);
	glBindTexture(GL_TEXTURE_2D, _gaussianNoiseTexture);
	SetupParams();

	//glGenTextures(1, &_phaseAmplitudeMap);
	//glBindTexture(GL_TEXTURE_2D, _phaseAmplitudeMap);
	//SetupParams();
	//
	//glGenTextures(1, &_oppositePhaseAmplitudeMap);
	//glBindTexture(GL_TEXTURE_2D, _phaseAmplitudeMap);
	//SetupParams();

	//glGenBuffers(1, &_frequencyDomainBuffer);
	glGenBuffers(1, &_baseFrequencyBuffer);

	glGenTextures(1, &_heightMap);
	glBindTexture(GL_TEXTURE_2D, _heightMap);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);


	PrepareWaveData();
}

void Water::LoadRenderer()
{
	RAssert(LoadMesh(static_cast<unsigned int>(OriginalTesselationLevel)));

	/// Buffers
	glGenVertexArrays(1, &_vao);
	glBindVertexArray(_vao);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _mesh->GetIBO());
	glBindBuffer(GL_ARRAY_BUFFER, _mesh->GetVBO());
	glVertexAttribPointer(0, 2, GL_FLOAT, false, 0, nullptr);
	glEnableVertexAttribArray(0);
	glBindVertexArray(0);
}

bool Water::LoadMesh(unsigned int tesselationLevel)
{
	TessellatedPlane* _tesPlane = new TessellatedPlane();
	_mesh = _tesPlane;
	return _tesPlane->UpdateMesh(tesselationLevel);
}

void Water::ComputeBasePhaseAmplitudes(GLuint h0, GLuint oppH0)
{
	Assert(glGetError() == GL_NO_ERROR);
	RAssert(_computeBaseWaveShader);
	_computeBaseWaveShader->Use();
	GLuint program = _computeBaseWaveShader->Program;
	Assert(glGetError() == GL_NO_ERROR);

	//glBindTexture(GL_TEXTURE_2D, _phaseAmplitudeMap);
	//glBindImageTexture(0, _phaseAmplitudeMap, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RG32F);
	//auto h0Loc = glGetUniformLocation(program, "h0");
	//RAssert(h0Loc != -1);
	//glUniform1i(h0Loc, 0);
	//Assert(glGetError() == GL_NO_ERROR);
	//
	//glBindTexture(GL_TEXTURE_2D, _oppositePhaseAmplitudeMap);
	//glBindImageTexture(1, _oppositePhaseAmplitudeMap, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RG32F);
	//auto oppH0Loc = glGetUniformLocation(program, "oppH0");
	//RAssert(oppH0Loc != -1);
	//glUniform1i(oppH0Loc, 1);
	//Assert(glGetError() == GL_NO_ERROR);

	glBindTexture(GL_TEXTURE_2D, _gaussianNoiseTexture);
	glBindImageTexture(2, _gaussianNoiseTexture, 0, GL_FALSE, 0, GL_READ_ONLY, GL_RGBA32F);
	auto gaussianNoiseTexLoc = glGetUniformLocation(program, "gaussianNoiseTex");
	RAssert(gaussianNoiseTexLoc != -1);
	glUniform1i(gaussianNoiseTexLoc, 2);

	OpenGLUtils::AddGLMUniform(program, "gridResolution", HeightMapSize, false);
	OpenGLUtils::AddGLMUniform(program, "windForce", glm::vec2(WindForce.GetValue().x, WindForce.GetValue().y));
	OpenGLUtils::AddGLMUniform(program, "waveHeightFactor", static_cast<float>(WaveHeightFactor));

	glBindBuffer(GL_SHADER_STORAGE_BUFFER, _baseFrequencyBuffer);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, _baseFrequencyBuffer);

	glDispatchCompute(HeightMapSize, HeightMapSize, 1);
	_baseFrequencyData = reinterpret_cast<BaseFrequencyDataItem*>(glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_READ_ONLY));
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
	glUseProgram(0);
	Assert(glGetError() == GL_NO_ERROR);
}

#include <math.h>
#define SWAP(a,b) tempr=(a);(a)=(b);(b)=tempr
void fourn(float data[], unsigned long nn[], int ndim, int isign)
{
	int idim;
	unsigned long i1, i2, i3, i2rev, i3rev, ip1, ip2, ip3, ifp1, ifp2;
	unsigned long ibit, k1, k2, n, nprev, nrem, ntot;
	float tempi, tempr;
	double theta, wi, wpi, wpr, wr, wtemp;
	for (ntot = 1, idim = 1; idim <= ndim; idim++)
		ntot *= nn[idim - 1];
	nprev = 1;
	for (idim = ndim; idim >= 1; idim--) {
		n = nn[idim - 1];
		nrem = ntot / (n*nprev);
		ip1 = nprev << 1;
		ip2 = ip1*n;
		ip3 = ip2*nrem;
		i2rev = 1;
		for (i2 = 1; i2 <= ip2; i2 += ip1) {
				if (i2 < i2rev) {
					for (i1 = i2; i1 <= i2 + ip1 - 2; i1 += 2) {
						for (i3 = i1; i3 <= ip3; i3 += ip2) {
							i3rev = i2rev + i3 - i2;
							SWAP(data[i3-1], data[i3rev-1]);
							SWAP(data[i3], data[i3rev]);
						}
					}
				}
			ibit = ip2 >> 1;
			while (ibit >= ip1 && i2rev > ibit) {
				i2rev -= ibit;
				ibit >>= 1;
			}
			i2rev += ibit;
		}
		ifp1 = ip1;
			while (ifp1 < ip2) {
				ifp2 = ifp1 << 1;
				theta = isign*6.28318530717959 / (ifp2 / ip1);
					wtemp = sin(0.5*theta);
				wpr = -2.0*wtemp*wtemp;
				wpi = sin(theta);
				wr = 1.0;
				wi = 0.0;
				for (i3 = 1; i3 <= ifp1; i3 += ip1) {
					for (i1 = i3; i1 <= i3 + ip1 - 2; i1 += 2) {
						for (i2 = i1; i2 <= ip3; i2 += ifp2) {
							k1 = i2;
							k2 = k1 + ifp1;
							tempr = static_cast<float>(wr)*data[k2 - 1] - static_cast<float>(wi)*data[k2];
							tempi = static_cast<float>(wr)*data[k2] + static_cast<float>(wi)*data[k2 - 1];
							data[k2 - 1] = data[k1 - 1] - tempr;
							data[k2] = data[k1] - tempi;
							data[k1 - 1] += tempr;
							data[k1] += tempi;
						}
					}
					wr = (wtemp = wr)*wpr - wi*wpi + wr;
						wi = wi*wpr + wtemp*wpi + wi;
				}
				ifp1 = ifp2;
			}
		nprev *= n;
	}
}

glm::vec2 ComplexMult(glm::vec2 a, glm::vec2 b)
{
	return glm::vec2(a.x * b.x - a.y * b.y, a.x * b.y + a.y * b.x);
}

void Water::ComputeHeightMap(double t, GLuint heightMap)
{
	RAssert(_computeHeightMapShader);
	static GLsync sync;
	/// Convert results (computed for prev frame)
	//if (_frequencyDomainBufferReady)
	//{
		//int totalMapSize = HeightMapSize * HeightMapSize;
		//glBindBuffer(GL_SHADER_STORAGE_BUFFER, _frequencyDomainBuffer);
		//
		//float* values = new float[totalMapSize * 2];
		////memset(values, 0.f, totalMapSize * 2);
		//Assert(GL_WAIT_FAILED != glClientWaitSync(sync, GL_SYNC_FLUSH_COMMANDS_BIT, 1000000000));
		//void* buff = glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_READ_WRITE);
		//memcpy(values, buff, totalMapSize * 2);
		//float* output = new float[totalMapSize * 2];


		/// Shader code
		/*ivec2 fGroup = ivec2(gl_NumWorkGroups.xy);
		float f2 = float((fGroup.x / 2 + int(gl_WorkGroupID.x)) % fGroup.x - fGroup.x / 2) / 1000.0 + 0.0000001;
		float f1 = float((fGroup.y / 2 + int(gl_WorkGroupID.y)) % fGroup.y - fGroup.y / 2) / 1000.0 + 0.0000001;

		vec2 K = vec2(f1, f2);

		//vec2 K = max(vec2(gl_WorkGroupID.xy) / 1000.0, 0.000001);

		ivec2 texCoords = ivec2(gl_WorkGroupID.xy);
		vec2 h0Val = imageLoad(h0, texCoords).xy;
		vec2 oppH0Val = imageLoad(oppH0, texCoords).xy;
		float omega = sqrt(9.8 * length(K));
		vec2 res = complexMult(h0Val, vec2(cos(omega * t), sin(omega * t)));
		res += complexMult(oppH0Val, vec2(cos(-omega * t), sin(-omega * t)));
		values[gl_WorkGroupID.x + gl_WorkGroupID.y * gl_NumWorkGroups.x] = res;*/

		int size = HeightMapSize;
		int totalMapSize = size * size;
		int halfSize = size / 2;
		float f2, f1, omegat;
		float* values = new float[size * size * 2];
		int vectorIndex = 0;
		glm::vec2 res;
		float TileSize = static_cast<float>(Size) / static_cast<float>(HeightMapTiling);
		for (int i = 0; i < size; i++)
		{
			f2 = static_cast<float>((halfSize + i) % size - halfSize) / WaterTileSize + 0.00000001f;
			for (int j = 0; j < size; j++)
			{
				vectorIndex = i + j * size;
				f1 = static_cast<float>((halfSize + j) % size - halfSize) / WaterTileSize + 0.00000001f;
				omegat = glm::sqrt(9.8 * glm::sqrt(f1 * f1 + f2 * f2)) * t;
				res = ComplexMult(_baseFrequencyData[vectorIndex].h0, glm::vec2(cos(omegat), sin(omegat)));
				res += ComplexMult(_baseFrequencyData[vectorIndex].oppH0, glm::vec2(cos(-omegat), sin(-omegat)));
				memcpy(values + (vectorIndex * 2), &res.x, 2 * sizeof(float));
			}
		}

		unsigned long sizes[2] = { size, size };
		fourn(values, sizes, 2, -1);
		//CFFT::Inverse(values, output, totalMapSize);
		glBindTexture(GL_TEXTURE_2D, _heightMap);
		float* heights = new float[totalMapSize];
		float invTotalMapSize = 1.f / static_cast<float>(totalMapSize);
		for (int i = 0; i < totalMapSize * 2; i+=2)
			heights[i >> 1] = glm::sqrt((values[i] * values[i] + values[i + 1] * values[i + 1]) * invTotalMapSize);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, HeightMapSize, HeightMapSize, 0, GL_RED, GL_FLOAT, heights);

		glBindTexture(GL_TEXTURE_2D, 0);
		//glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
		//glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

		delete[] heights;
		delete[] values;
	//}
	//else
	//{
	//	glBindTexture(GL_TEXTURE_2D, _heightMap);
	//	glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, HeightMapSize, HeightMapSize, 0, GL_RED, GL_FLOAT, nullptr);
	//}

	///// Compute results for next frame.
	//_computeHeightMapShader->Use();
	//GLuint program = _computeHeightMapShader->Program;

	//glBindTexture(GL_TEXTURE_2D, _heightMap);
	//glTexStorage2D(GL_TEXTURE_2D, 1, GL_R8, HeightMapSize, HeightMapSize);
	//glBindImageTexture(0, _heightMap, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_R8);
	//auto heightMapLoc = glGetUniformLocation(program, "heightMap");
	//RAssert(heightMapLoc != -1);
	//glUniform1i(heightMapLoc, 0);

	//glBindImageTexture(1, _phaseAmplitudeMap, 0, GL_FALSE, 0, GL_READ_ONLY, GL_RG32F);
	//auto phaseAmplitudeLoc = glGetUniformLocation(program, "h0");
	//RAssert(phaseAmplitudeLoc != -1);
	//glUniform1i(phaseAmplitudeLoc, 1);
	//
	//glBindImageTexture(2, _oppositePhaseAmplitudeMap, 0, GL_FALSE, 0, GL_READ_ONLY, GL_RG32F);
	//auto oppositePhaseAmplitudeMapLoc = glGetUniformLocation(program, "oppH0");
	//RAssert(oppositePhaseAmplitudeMapLoc != -1);
	//glUniform1i(oppositePhaseAmplitudeMapLoc, 2);
	//
	//auto tLoc = glGetUniformLocation(program, "t");
	//glUniform1f(tLoc, static_cast<GLfloat>(GAME.GetFrameTime()));
	//OpenGLUtils::AddGLMUniform(program, "gridResolution", HeightMapSize, false);
	//
	///// Compute
	//glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, _frequencyDomainBuffer);
	//glDispatchCompute(HeightMapSize, HeightMapSize, 1);
	//sync = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);
	//_frequencyDomainBufferReady = true;
	//
	//glUseProgram(0);
	Assert(glGetError() == GL_NO_ERROR);
}

void Water::Render(Shader const* shader, int pass)
{
	GameObject::Render(shader, pass);

	/// Model matrix
	auto modelLoc = glGetUniformLocation(shader->Program, "model");
	glm::mat4 worldTransform = GetWorldTransformation();
	float size = static_cast<float>(Size.GetValue());
	float height = static_cast<float>(Height.GetValue());
	glm::mat4 terrainTransform = glm::mat4(
		size, 0.f, 0.f, 0.f,
		0.f, height, 0.f, 0.f,
		0.f, 0.f, size, 0.f,
		worldTransform[0][3], worldTransform[1][3], worldTransform[2][3], 1.f);
	glUniformMatrix4fv(modelLoc, 1, false, glm::value_ptr(terrainTransform));

	auto normalPrecisionLoc = glGetUniformLocation(shader->Program, "normalPrecision");
	glUniform1f(normalPrecisionLoc, static_cast<float>(NormalPrecision) / 1000.f);

	auto tessellationRatioLoc = glGetUniformLocation(shader->Program, "tessellationRatio");
	glUniform1f(tessellationRatioLoc, static_cast<float>(TessellationRatio));

	auto colorLoc = glGetUniformLocation(shader->Program, "mat.albedo");
	OpenGLUtils::AddGLMUniform(colorLoc, Color);
	OpenGLUtils::AddGLMUniform(shader->Program, "mat.roughness", static_cast<float>(Roughness), false);

	/// Textures binding
	auto heightmapLoc = glGetUniformLocation(shader->Program, "heightMap");

	OpenGLUtils::AddGLMUniform(shader->Program, "heightMapTiling", HeightMapTiling);

	glUniform1i(heightmapLoc, 0);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, _heightMap);
	
	auto albedoLoc = glGetUniformLocation(shader->Program, "albedo");
	
	glUniform1i(albedoLoc, 1);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, _heightMap);

	//glBindTexture(GL_TEXTURE_2D, _gaussianNoiseTexture);
	//glBindImageTexture(2, _gaussianNoiseTexture, 0, GL_FALSE, 0, GL_READ_ONLY, GL_RGBA8);
	//auto gaussianNoiseTexLoc = glGetUniformLocation(shader->Program, "gaussianNoiseTex");
	//RAssert(gaussianNoiseTexLoc != -1);
	//glUniform1i(gaussianNoiseTexLoc, 2);

	/// Array buffer
	glBindVertexArray(_vao);
	glEnableVertexAttribArray(0);

	if (Wireframe.GetValue())
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	/// Draw
	glPatchParameteri(GL_PATCH_VERTICES, 4);
	glDrawElements(GL_PATCHES, _mesh->GetIndicesCount(), GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	//glDisableVertexAttribArray(0);
	Assert(glGetError() == GL_NO_ERROR);
}