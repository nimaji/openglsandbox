#include "BoardPool.hpp"

#include <string>
#include <algorithm>
#include <memory>

#include "tinyxml.h"
#include "Debug.hpp"

#include "PersistentBoard.hpp"

#include "IO/FileIO.hpp"

BoardPool::BoardPool()
{
}

BoardPool::~BoardPool()
{
}

void BoardPool::GenerateDocument(TiXmlDocument& document) const
{
	TiXmlDeclaration * decl = new TiXmlDeclaration( "1.0", "", "" );
	_document->LinkEndChild(decl);

	TiXmlElement* boardPool = new TiXmlElement("BoardPool");
	for (auto&& mementoPair : _pool)
	{
		TiXmlElement* element = new TiXmlElement("Board");
		
		element->SetAttribute("Name", mementoPair.first);
		mementoPair.second->Save(element);
		boardPool->LinkEndChild(element);
	}
	_document->LinkEndChild(boardPool);
}

void BoardPool::GenerateDocument()
{
	if (_isDirty)
	{
		_document = std::unique_ptr<TiXmlDocument>(new TiXmlDocument());
		GenerateDocument(*_document);
		_isDirty = false;
	}
}

bool BoardPool::SaveDocument(std::string const& fileName)
{
	GenerateDocument();
	return _document && _document->SaveFile(fileName);
}

bool BoardPool::Load(const char* name)
{
	std::string strName = FileIO::TransformToCurrentPlateform(name);
	name = strName.c_str();
	TiXmlDocument doc(name);
	bool loadOkay = doc.LoadFile();
	if (loadOkay)
		Log("%s successfully loaded.", name);
	else
		RELog(false, "Couldn't load the xml file %s.", name);

	auto bpElement = doc.FirstChildElement();
	RAssert(bpElement, false);
	for (auto child = bpElement->FirstChildElement(); child != nullptr; child = child->NextSiblingElement())
	{
		PersistentBoard* board = new PersistentBoard();
		board->Load(child->ToElement());

		std::string name;
		if (child->QueryStringAttribute("Name", &name) != TIXML_SUCCESS)
		{
			ELog("%s board is ill formed, couldn't retrieve it's name. Skipping.");
			continue;
		}
		AddBoard(name, board, true);
	}
	return true;
}

void BoardPool::AddBoard(std::string const& key, PersistentBoard* board, bool ignoreDirty)
{
	VRAssert(board);
	auto val = _pool.find(key);
	if (val == _pool.end())
	{
		_pool[key] = board;
		board->OnDirty.AddListener([this](){ this->Dirty(); });
		if (!ignoreDirty)
			Dirty();
	}
	else
		VRELog("Tried to add a already existing value.");

}

void BoardPool::UpdatePersistantBoard(std::string const& keyName, PersistentBoard* board, bool ignoreDirty)
{
	VRAssert(board);
	board->OnDirty.AddListener([this](){ this->Dirty(); });

	auto val = _pool.find(keyName);
	if (val != _pool.end())
	{
		board->UpdateFrom(*val->second);
		delete val->second;
		val->second = board;
	}
	else
	{
		_pool[keyName] = board;
	}

	if (!ignoreDirty)
		Dirty();
}

void BoardPool::Dirty()
{
	_isDirty = true;
}