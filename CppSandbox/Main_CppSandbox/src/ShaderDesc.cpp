#include "ShaderDesc.hpp"

#include "Rendering/Shader.hpp"

ShaderDesc::ShaderDesc(char const* shaderName)
:	_name(shaderName)
{
}


ShaderDesc::~ShaderDesc()
{
	if (_shader)
		delete _shader;
}

void ShaderDesc::LoadShader(char const* vertexShader, char const* fragmentShader)
{
	if (_shader)
		delete _shader;

	_shader = Shader::CreateShader(vertexShader, fragmentShader);
}

void ShaderDesc::SetShader(Shader* shader)
{
	if (_shader)
		delete _shader;

	_shader = shader;
}

bool ShaderDesc::IsValid()
{
	return _shader && _name;
}
