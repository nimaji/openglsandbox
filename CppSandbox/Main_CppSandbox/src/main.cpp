#include <iostream>
#include <cmath>

// GLM Mathematics
#include <glm/glm.hpp>

// Other includes
#include "Rendering/Shader.hpp"

//Config
#include "ConfigManager.hpp"

//Editor
#include <imgui.h>
#include "AssetsManager.hpp"
#include "Editor/Editor.hpp"

//System
#include "System.hpp"

//Rendering
#include "Rendering/RenderingContext.hpp"
#include "Rendering/RenderingPipeline.hpp"

// Input
#include "IO/Input.hpp"

//Game
#include "Game.hpp"
#include "GameObject.hpp"
#include "AIObject.hpp"
#include "CubeMapGO.hpp"
#include "ShaderDesc.hpp"
#include "Spline.hpp"

#include "Patch.hpp"
#include "Water.hpp"
#include "Terrain.hpp"
#include "Random.hpp"

#include "Debug.hpp"

#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>     // Post processing flags

#include "IO/FileIO.hpp"

//Plateform specific
#ifdef _WIN32
#include "Rendering/OpenGL/OpenGLRenderingContext.hpp"
#include "Rendering/OpenGL/OpenGLRenderingPipeline.hpp"
#include "IO/Windows/GLFWInput.hpp"
#elif _ORBIS
#include "Memory/MemoryManager.hpp"
#include "Rendering/PS4/PS4RenderingContext.hpp"
#include "Rendering/PS4/PS4RenderingPipeline.hpp"
#include "IO/PS4/PS4Input.hpp"
#else
Unimplemented platform.
#endif


Input*				input = nullptr;
RenderingContext*	renderingContext = nullptr;
RenderingPipeline*	renderingPipeline = nullptr;

glm::vec3 cubePositions[] = {
	glm::vec3(0.0f,  0.0f,  0.0f),
	glm::vec3(2.0f,  5.0f, -15.0f),
	glm::vec3(-1.5f, -2.2f, -2.5f),
	glm::vec3(-3.8f, -2.0f, -12.3f),
	glm::vec3(2.4f, -0.4f, -3.5f),
	glm::vec3(-1.7f,  3.0f, -7.5f),
	glm::vec3(1.3f, -2.0f, -2.5f),
	glm::vec3(1.5f,  2.0f, -2.5f),
	glm::vec3(1.5f,  0.2f, -1.5f),
	glm::vec3(-1.3f,  1.0f, -1.5f)
};

#pragma endregion

void LoadScene(aiScene const* scene)
{
	GAME.scene = new Scene(scene);
	new AIObject(GAME.rootGO, scene->mRootNode);
}

bool LoadScene(const std::string& pFile)
{
	// Create an instance of the Importer class
	Assimp::Importer* importer = new Assimp::Importer();
	// And have it read the given file with some example postprocessing
	// Usually - if speed is not the most important aspect for you - you'll 
	// propably to request more postprocessing than we do in this example.
	std::string convertedPFile = FileIO::TransformToCurrentPlateform(pFile);
	const aiScene* scene = importer->ReadFile(convertedPFile,
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_FlipUVs |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType);

	// If the import failed, report it
	if (!scene)
	{
		std::cout << importer->GetErrorString();
		return false;
	}

	std::cout << "Scene : " << convertedPFile << " model was successfully imported" << std::endl;

	LoadScene(scene);

	// We're done. Everything will be cleaned up by the importer destructor
	return true;
}

void InitScene()
{
	GAME.rootGO = new GameObject("RootGameObject");
	auto go = new CubeMapGO("CubeMap", GAME.rootGO);
	//LoadScene("Assets/Scene/Cube_Test_NIm.STL");
	//LoadScene("Assets/Model/Wood_Branch_paErt_4K_3d_ms/Aset_wood_branch_S_paErt_LOD0.fbx");
	LoadScene("Assets/Scene/SandboxScene.fbx");
	//new Spline("Spline", GAME.rootGO);
	//new Patch("Patch", GAME.rootGO);
	//new Terrain("Terrain", GAME.rootGO);
	//new Water("Ocean", GAME.rootGO);
}

void Init()
{
	// Random system init
	RANDOM.Initialize();
	SYSTEM.Initialize();

#ifdef _WIN32
	renderingContext = new OpenGLRenderingContext();
	RenderingContext::sSetCurrent(renderingContext);
	renderingPipeline = new OpenGLRenderingPipeline();

	input = new GLFWInput();
#elif _ORBIS
	MEMORY_MANAGER.Initialize();
	renderingContext = new PS4RenderingContext();
	RenderingContext::sSetCurrent(renderingContext);
	renderingPipeline = new PS4RenderingPipeline();

	input = new PS4Input();
#else
	Unimplemented platform.
#endif

	RenderingContextDesc contextDesc;
	contextDesc.Name = "CppSandbox";
#ifdef _ORBIS
	contextDesc.Width = 1920;
	contextDesc.Height = 1080;
#else
	contextDesc.Width = 1680;
	contextDesc.Height = 1050;
#endif
	Assert(renderingContext);
	Assert(renderingPipeline);
	if (!renderingContext || !renderingPipeline)
	{
		ELog("Couldn't load the renderer.");
		return;
	}
	renderingContext->Initialize(contextDesc);
	renderingPipeline->Initialize();

	input->Initialize();
	INPUT->DoHideCursor(true);

	EDITOR.Initialize();

	CONFIG_MGR.LoadConfigFile("config.xml");

	ASSETS_MANAGER.Init();
	GAME.Init();

	InitScene();

	GAME.LoadSave("main.scene");
}

void DebugMaterial(aiMaterial const& mat)
{
	for (size_t i = 0; i < mat.mNumProperties; i++)
	{
		auto& property = *mat.mProperties[i];
		std::string text = property.mKey.C_Str();
		switch (property.mType)
		{
		case aiPTI_Float:
			for (int i = 0; i < (property.mDataLength) / sizeof(float); i++)
				text += "   " + std::to_string(*((float*)property.mData + i));
			ImGui::Text(text.c_str());
		break;
		case aiPTI_String:
			ImGui::Text((std::string(property.mKey.C_Str()).c_str()));
		break;
		case aiPTI_Integer:
			for (int i = 0; i < (property.mDataLength) / sizeof(int); i++)
				text += "   " + std::to_string(*((int*)property.mData + i));
			ImGui::Text(text.c_str());
		break;
		case aiPTI_Buffer:
			ImGui::Text(property.mKey.C_Str());
		break;
		default:
			ImGui::Text(property.mKey.C_Str());
		break;
		}
	}
}

void DebugMaterials()
{
	using namespace ImGui;

	if (TreeNode("Materials"))
	{
		aiScene const* scene = GAME.scene->GetAiScene();
		for (unsigned int i = 0; i < scene->mNumMaterials; i++)
		{
			using namespace ImGui;
			using namespace std;
			aiString matName;
			if (scene->mMaterials[i]->Get(AI_MATKEY_NAME, matName) != aiReturn_SUCCESS)
				matName = ("material " + to_string(i)).c_str();
				
			if (TreeNode(matName.C_Str()))
			{
				DebugMaterial(*scene->mMaterials[i]);
				TreePop();
			}
		}
		TreePop();
	}
}

int i = 0;
void Update()
{
	if (input)
		input->Update();

	if (INPUT->CloseAction.WasTriggered())
		SYSTEM.AskCloseApplication();

	GAME.Update();
	EDITOR.NewFrame();
	if (EDITOR.IsEnabled())
	{
		ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
		GAME.DebugUpdate();
		DebugMaterials();
	}
}

void Render()
{
	renderingPipeline->Render(renderingContext, EDITOR.IsEnabled());
}

void Shutdown()
{
	EDITOR.Shutdown();
	// Terminate GLFW, clearing any resources allocated by GLFW.

	if (input)
	{
		input->Shutdown();
		delete input;
		input = nullptr;
	}
	if (renderingPipeline)
	{
		renderingPipeline->Shutdown();
		delete renderingPipeline;
		renderingPipeline = nullptr;
	}
	if (renderingContext)
	{
		renderingContext->Shutdown();
		delete renderingContext;
		renderingContext = nullptr;
	}

	#ifdef _ORBIS
	MEMORY_MANAGER.Shutdown();
	#endif

	SYSTEM.Shutdown();
}

int main()
{
	Init();

	while (!SYSTEM.ApplicationShouldClose())
	{
		Update();
		Render();
	}

	Shutdown();
	return 0;
}
