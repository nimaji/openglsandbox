#pragma once

#include <glm/glm.hpp>
#include "IO/InputAction.hpp"

#define INPUT Input::GetCurrent()

class Input
{
public:
	static Input* GetCurrent();
	static void SetCurrent(Input* current);

	virtual void Initialize() = 0;
	virtual void Update() = 0;
	virtual void Shutdown() = 0;
	virtual void DoHideCursor(bool doHideCursor) = 0;

	bool TestValidity() const;

	glm::dvec2	CursorPos;
	glm::dvec2	CursorMove;
	glm::dvec2	ViewAxis;
	glm::vec3	MoveAxis;

	InputAction CloseAction;
	InputAction DebugAction;
	InputAction SaveAction;
	InputAction FastMoveAction;
	InputAction SlowMoveAction;

protected:

	static const int _actionsCount = 5;
	InputAction* _actions[_actionsCount] = { &CloseAction, &DebugAction, &SaveAction, &FastMoveAction, &SlowMoveAction };
	bool _moveInputs[6] = {};

private:
	static Input* _current;
};