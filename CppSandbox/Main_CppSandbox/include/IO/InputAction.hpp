#pragma once

class InputAction
{
public:
	void SetTriggered();
	void SetUntriggered();
	void FrameClear();
	void Clear();

	bool WasTriggered() const { return _isTriggered; }
	bool IsUntriggered() const { return _isUntriggered; }
	bool IsRequested() const{ return _isRequested; }

private:
	bool _isTriggered = false;
	bool _isUntriggered = true;
	bool _isRequested = false;
};