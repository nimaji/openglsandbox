#pragma once

#include <iostream>
#include <string>
#include <cassert>

#ifdef _DEBUG
#define Log(format, ...) printf(format "\n", ##__VA_ARGS__)
#define WLog(format, ...) printf("Warning: " format "\n", ##__VA_ARGS__)
#define ELog(format, ...) printf("ERROR: " format "\n", ##__VA_ARGS__)
#define Assert(condition) assert(condition)
#else
#define Log(format, ...)
#define WLog(format, ...)
#define ELog(format, ...)
#define Assert(condition)
#endif

#define RLog(ret, format, ...) { Log(format, ##__VA_ARGS__); return ret; }
#define VRLog(format, ...) RLog( , format, ##__VA_ARGS__)

#define RELog(ret, format, ...) { ELog(format, ##__VA_ARGS__); return ret; }
#define VRELog(format, ...) RELog( , format, ##__VA_ARGS__)

#define RWLog(ret, format, ...) { WLog(format, ##__VA_ARGS__); return ret; }
#define VRWLog(format, ...) RWLog( , format, ##__VA_ARGS__)

#define RAssert(condition, rvalue) { Assert(condition); if (!(condition)) return rvalue; }
#define VRAssert(condition) RAssert(condition, )

#define SAssert(condition) static_assert(condition)