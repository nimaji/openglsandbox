#pragma once

#include <map>
#include <string>
#include <vector>

#include "Rendering/Resources/Mesh.hpp"
#include "Rendering/Resources/Texture.hpp"

class ResourceThread;

class ResourceManager
{
public:
	~ResourceManager();

	Texture*		CreateTexture();
	Mesh*			CreateMesh();

	Texture*		LoadTexture(std::string const& path, bool linearSpace = false);
	Texture*		LoadTextureAsync(std::string const& path, bool linearSpace);
	Texture*		LoadCubemap(std::string const& path);

	Texture*		GetTexture(std::string const& path) const;
	bool			IsTextureLoaded(std::string const& path) const;

	void			UpdateThreadsStatus();

private:
	Texture*		GenTextureAsync(std::string const& path, bool linearSpace);
	Texture*		AddToTextureMap(std::string const&, Texture* texture, bool forceUpdate = false);

	void			JoinThreads();
	void			AddResourceThread(ResourceThread* thread);

	std::map<std::string, Texture*>			_textureMap;

	std::vector<ResourceThread*>			_resourceThreads;

};
