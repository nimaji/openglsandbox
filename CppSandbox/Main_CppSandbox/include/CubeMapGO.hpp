#pragma once

#include "GameObject.hpp"
#include <string>

class CubeMapGO :
	public GameObject
{
public:
	CubeMapGO(std::string const& name, GameObject* parent);
	~CubeMapGO();
};

