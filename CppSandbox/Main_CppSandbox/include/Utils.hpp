#pragma once

#include <vector>
#include <algorithm>
#include <stdio.h>
#include <sys/stat.h>

namespace Utils
{
	template<typename Searched, typename IT>
	Searched* GetObjectOfType(IT firstIt, IT lastIt)
	{
		auto res = std::find_if(firstIt, lastIt, [](typename std::iterator_traits<IT>::valueType x) { return dynamic_cast<Searched*>(x); });
		return (res != lastIt) ? (Searched*)(*res) : nullptr;
	}

	template<typename TRet, typename IT, typename PRED>
	std::vector<TRet*> GetObjectsIf(IT firstIt, IT lastIt, PRED const& predicate)
	{
		std::vector<TRet*> res;
		for (; firstIt != lastIt; firstIt++)
		{
			if (predicate(*firstIt))
				res.push_back(static_cast<TRet*>(*firstIt));
		}
		return res;
	}

	template<typename T, typename TRet = T, typename IT>
	std::vector<TRet*> GetObjectsOfType(IT firstIt, IT lastIt)
	{
		return GetObjectsIf<TRet>(firstIt, lastIt, [](typename std::iterator_traits<IT>::value_type obj)
		{
			return dynamic_cast<T*>(obj);
		});
	}

	namespace Strings
	{
		std::string ToLower(std::string const& str);
	} // namespace Strings
}

