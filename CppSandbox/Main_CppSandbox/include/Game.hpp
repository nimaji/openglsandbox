#pragma once

#include "GameObject.hpp"
#include "Scene.hpp"
#include "Utils.hpp"

#include "BoardPool.hpp"

#include "LightGO.hpp"
#include "DirectionalLightGO.hpp"
#include "Camera.hpp"
#include "ResourceManager.hpp"

class Object;
class Input;

#define GAME Game::GetInstance()
class Game
{
public:
	Game(Game const&) = delete;
	Game(Game&&) = delete;
	
	Game& operator=(Game&&) = delete;
	Game& operator=(Game const&) = delete;

	~Game();

	static Game& GetInstance();

	void Init();

	void Update();
	void DebugUpdate();
	void SaveDefault();

	/// Load the game from the savePath, filing Objects values with saved one, 
	/// and save the path as the save destination.
	/// Made to be called one at the start of the application, not tested on multiple calls.
	void LoadSave(std::string const& savePath);

	Scene*							scene = nullptr;
	GameObject*						rootGO = nullptr;
	std::vector<Object*>			objects;
	std::vector<GameObject*>		gameObjects;
	bool							isImGUIEnabled = false;

	BoardPool						boardPool;
	ResourceManager					resourceManager;

	DirectionalLightGO*				directional = nullptr;
	LightGO*						ambiant = nullptr;
	Camera*							camera = nullptr;

	/// Returns the time of the current frame.
	double GetFrameTime() const { return _lastFrameTime; };
	/// Returns the delta time of the last frame. 
	/// Used to time the current frame.
	double GetDeltaTime() const { return _deltaTime; };

	template<typename T>
	T* GetObjectOfType() const
	{
		return Utils::GetObjectOfType<T>(objects.begin(), objects.end());
	}

	template<typename T, typename TRet = T>
	std::vector<TRet*> GetObjectsOfType()
	{
		return Utils::GetObjectsOfType<T, TRet>(objects.begin(), objects.end());
	}

	template<typename TRet = Object>
	std::vector<TRet*> GetObjectsIf(std::function<bool(Object*)> const& predicate)
	{
		return Utils::GetObjectsIf<TRet>(objects.begin(), objects.end(), predicate);
	}

	template<typename T = Object>
	T* GetObjectOfFullName(std::string const& name)
	{
		auto res = std::find_if(objects.begin(), objects.end(),
		[&name](Object* obj)
		{
			return obj->GetFullName() == name;
		});

		if (res == objects.end())
			return nullptr;
		else
			return static_cast<T*>(*res);
	}

private:
	Game();

	void UpdateView(Input& input);

	/// Returns the time from the game initialization.
	double GetTime() const;

	// Time
	double _startTime = -1.f;  	// Time of last frame
	double _deltaTime = 0.0f;	// Time between current frame and last frame
	double _lastFrameTime = 0.0f;  	// Time of last frame

	std::string _loadedSavePath = "defaultSave.scene";
};

