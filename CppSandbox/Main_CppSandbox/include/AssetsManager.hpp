#pragma once

#include <map>
#include <string>

#include "Asset.hpp"

#define ASSETS_MANAGER AssetsManager::GetInstance()

class AssetsManager
{
public:
	~AssetsManager();

	static AssetsManager& GetInstance();

	void Init();
	void LoadAssets();


private:
	std::string GetAssetsPath();
	void AddRecursivelyAssetsFromPath(std::string const& path);

	Asset::Type FindFileType(std::string const & path);

	AssetsManager();
	std::map<std::string, Asset*> _assets;
};

