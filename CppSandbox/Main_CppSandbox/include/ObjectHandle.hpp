#pragma once

#include <string>

class Object;


/// Keep a persistant handle on the object.
/// Used to keep track of the object threw different game sessions.
class ObjectHandle
{
public:
					ObjectHandle() = default;
					ObjectHandle(Object* obj);
					ObjectHandle(std::string const& obj);
					~ObjectHandle();

	Object*			GetObject() { return _obj; };
	std::string		GetSerializableValue() const;

private:
	Object*			RetrieveObject();

	std::string		_name;
	Object*			_obj = nullptr;
};

