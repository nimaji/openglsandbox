#pragma once
#include "GameObject.hpp"

class Spline :
	public GameObject
{
	enum SplineType
	{
		Bezier = 0,
		BSpline
	};

public:
	Spline(std::string const& name, GameObject* parent);
	~Spline();

//=>	virtual void Spline::Render(Shader const* shader, int pass) override;
	virtual void Update() override;
	virtual void DebugUpdate() override;

	SplineType GetSplineType() { return (SplineType)_persistentBoard->QuickGetIntVal("SplineType"); }

protected:
	virtual void Initialize() override;

private:
	float* PointsToFloatArray() const;


	void UpdateIBO();
	void UpdateIBO(SplineType type, int pointCount);
//=>	GLuint GetIndexCount() const;
//=>	GLuint GetIndexCount(SplineType type, int pointCount) const;
	static const int _pointCount = 10;
};

