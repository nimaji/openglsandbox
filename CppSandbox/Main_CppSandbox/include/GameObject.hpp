#pragma once

#include <assimp/scene.h>
#include <vector>
#include <string>
#include <algorithm>

#include "glm/glm.hpp"

#include "Utils.hpp"

#include "BoardPool.hpp"
#include "PersistentBoard.hpp"
#include "Transform.hpp"
#include "Object.hpp"

#include "Rendering/Material.hpp"

class Mesh;
class Shader;
//class Material;

class GameObject : public Object
{
public:
	GameObject(std::string const& name, GameObject* parent = nullptr);
	virtual ~GameObject();

	/// The function is called just before the rendering of all the materials
	/// registered to the pipeline.
	virtual void		PreRender() {};

	virtual void		Initialize() override;
	virtual void		Update() override;
	virtual void		DebugUpdate() override;

	unsigned short GetShaderIndex() { return _shaderIndex; }
	void SetShaderIndex(unsigned short index) { _shaderIndex = index; }

	void _InternalDebugUpdate();

	template<typename T>
	T* GetChildrenOfType() const
	{
		return Utils::GetObjectOfType<T>(_children.begin(), _children.end());
	}

	template<typename T>
	std::vector<T*> GetChildrensOfType()
	{
		return Utils::GetObjectsOfType<T>(_children.begin(), _children.end());
	}

	RVAR(glm::vec3, Position, glm::vec3());
	RVAR(Material*, BaseMaterial, nullptr);

protected:
	virtual std::string		GetFullName() override;

	glm::mat4	GetWorldTransformation();

	Transform					_transform;
	GameObject*					_parent = nullptr;
	std::vector<GameObject*>	_children;

	unsigned short				_shaderIndex = -1;

	// debug
	bool						_isMenuOpened = true;
};

