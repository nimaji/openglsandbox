#pragma once

#include "tinyxml.h"
#include <map>
#include <vector>

#define CONFIG_MGR ConfigManager::GetInstance()

using AssetTypeExtentionMap = std::map<std::string, std::vector<std::string>>;
using CubemapFolderList = std::vector<std::pair<std::string, std::string>>;
class ConfigManager
{
public:
							ConfigManager(ConfigManager const&) = delete;
							ConfigManager(ConfigManager&&) = delete;

	ConfigManager&			operator=(ConfigManager&&) = delete;
	ConfigManager&			operator=(ConfigManager const&) = delete;

							~ConfigManager();

	static ConfigManager&	GetInstance();

	bool					LoadConfigFile(const char* filename);

	std::string				GetAssetsPath() const;
	AssetTypeExtentionMap	GetAssetTypeExtentions() const;
	CubemapFolderList		GetAvailableCubemaps() const;

private:
	ConfigManager();

	void	AddCubemapFolderIfValid(std::string const& path, CubemapFolderList& vector) const;
	
	class TiXmlDocument* _currentDocument = nullptr;

	class TiXmlElement* GetRootElement(std::string const& name) const;
};
