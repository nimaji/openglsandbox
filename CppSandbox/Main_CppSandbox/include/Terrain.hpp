#pragma once
#include "GameObject.hpp"

#include "AstImage.hpp"

class Terrain :
	public GameObject
{
public:
	Terrain(std::string const& name, GameObject* parent);
	~Terrain();

	virtual void Initialize() override;

	/// Create and load the terrain mesh.
	/// tesselationLevel represent the number of square on an edge.
	//->bool LoadMesh(GLuint tesselationLevel);

	//->virtual void Render(Shader const* shader, int pass) override;

	/// Draw the terrain in wireframe.
	RVAR(bool, Wireframe, false);

	/// The height map of the terrain.
	RVAR(AstImage*, HeightMap, nullptr);
	/// The main texture of the terrain
	RVAR(AstImage*, TextureUp, nullptr);
	RVAR(AstImage*, TextureDown, nullptr);
	RVAR(AstImage*, TextureCliff, nullptr);


	RVAR(double, NormUpValue, 0.7);
	RVAR(double, SlopeUpCos, 0.1);
	RVAR(double, CliffUpTexBlend, 0.2);

	RVAR(double, UpDownBlend, 0.1);

	RVAR(double, SlopeDownCos, 0.1);
	RVAR(double, CliffDownTexBlend, 0.2);

	/// The tiling of the texture multiplicator
	RVAR(int, TextureTiling, 20);

	/// The tiling of the heightmap.
	RVAR(int, HeightMapTiling, 1);

	/// The size of any edge. The mesh will be updated accordingly.
	RVAR(double, Size, 20.0);
	/// The height for the value 1 of the heightmap.
	RVAR(double, Height, 20.0);

	/// The original tesselation level.
	/// The index means the number of squares of any edge,
	/// so a value of 10 means 100 squares in total.
	RVAR(int, OriginalTesselationLevel, 20);

	/// The tessellation ratio. Decide the amount of tessellation done to the object.
	/// The tessellation is in tessellation value/projected line pixels
	RVAR(double, TessellationRatio, 0.1);

	/// The precision of the normals sampled from the height map.
	/// Correspond to the normalized coordinate offset used to 
	/// probe the heightmap multiplied by 1000.
	RVAR(double, NormalPrecision, 5);

private:
	void LoadRenderer();

	//->GLuint _heightMap;
	//->GLuint _textureUp;
	//->GLuint _textureDown;
	//->GLuint _textureCliff;
};

