#pragma once

#define EDITOR Editor::GetInstance()

class Editor
{
public:
	Editor(Editor const&) = delete;
	Editor(Editor&&) = delete;

	Editor& operator=(Editor const&) = delete;
	Editor&& operator=(Editor&&) = delete;

	static Editor& GetInstance();

	void Initialize();
	void NewFrame();
	void Shutdown();

	void Toggle();
	bool IsEnabled() const { return _enabled; }
	void SetEnabled(bool enabled);
	

private:
	Editor() = default;

	bool _enabled = false;
};

