#pragma once

#include <map>
#include <string>

#include "tinyxml.h"
#include "Debug.hpp"
#include "Delegate.hpp"

#include <glm/glm.hpp>

#include "RVHandle.hpp"

#include "ObjectHandle.hpp"
#include "RuntimeVariable.hpp"

#include "tinyxml.h"

class Object;

class PersistentBoard
{
public:
	PersistentBoard();
	~PersistentBoard();

	void SaveRuntimeVar(TiXmlElement* element, RuntimeVariable const& var) const;
	void Save(TiXmlElement* element);

	void LoadRuntimeVar(TiXmlElement const * element);
	void Load(TiXmlElement const * element);

	void UpdateFrom(PersistentBoard const& from);

	bool IsDirty() { return _isDirty; }

	bool GetVal(std::string const& key, RuntimeVariable*& value) const;
	bool AddVal(std::string const& key, RuntimeVariable* value);
	RuntimeVariable* AddVal(RuntimeVariable* value);

	void DebugUpdate();

	template <typename T>
	void QuickSetVal(std::string const& key, T value)
	{
		RuntimeVariable* var;
		GetVal(key, var);
		VRAssert(var);
		var->SetValue(value);
	}

	double		QuickGetDoubleVal(std::string const& key) const;
	int			QuickGetIntVal(std::string const& key) const;
	std::string QuickGetStringVal(std::string const& key) const;
	glm::vec3	QuickGetVec3Val(std::string const& key) const;
	glm::vec4	QuickGetVec4Val(std::string const& key) const;
	bool		QuickGetBoolVal(std::string const& key) const;
	Object*		QuickGetObjectVal(std::string const& key) const;

	Delegate<void> OnDirty;

private:
	std::map<std::string, RuntimeVariable*> _mapRuntimeVars;
	std::vector<RuntimeVariable*> _vecRuntimeVars;

	bool _isDirty = false;

	void Dirty();

	template <typename T>
	void SaveRuntimeVarToXml(TiXmlElement* element, T const& value) const
	{
		element->SetAttribute("Value", value);
	}

	template<typename T>
	bool SetRuntimeValFromElement(TiXmlElement const* element, RuntimeVariable& rVar) const
	{
		RAssert(element, false);
		T val;
		if (element->QueryValueAttribute<T>("Value", &val) == TIXML_SUCCESS)
		{
			rVar.SetValue(val, true);
			return true;
		}

		return false;
	}
};


template<>
void PersistentBoard::SaveRuntimeVarToXml<double>(TiXmlElement* element, double const& value) const;
template<>
void PersistentBoard::SaveRuntimeVarToXml<glm::vec3>(TiXmlElement* element, glm::vec3 const& value) const;
template<>
void PersistentBoard::SaveRuntimeVarToXml<glm::vec4>(TiXmlElement* element, glm::vec4 const& value) const;
template<>
void PersistentBoard::SaveRuntimeVarToXml<ObjectHandle>(TiXmlElement* element, ObjectHandle const& value) const;
template<>
bool PersistentBoard::SetRuntimeValFromElement<std::string>(TiXmlElement const* element, RuntimeVariable& rVar) const;
template<>
bool PersistentBoard::SetRuntimeValFromElement<glm::vec3>(TiXmlElement const* element, RuntimeVariable& rVar) const;
template<>
bool PersistentBoard::SetRuntimeValFromElement<glm::vec4>(TiXmlElement const* element, RuntimeVariable& rVar) const;
template<>
bool PersistentBoard::SetRuntimeValFromElement<ObjectHandle>(TiXmlElement const* element, RuntimeVariable& rVar) const;