#pragma once
#include "GameObject.hpp"

#include "AstImage.hpp"
#include "AstModel.hpp"
#include "AstShader.hpp"

struct BaseFrequencyDataItem
{
	glm::vec2 h0;
	glm::vec2 oppH0;
};

/// A water surface.
/// The waves are computed following http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.161.9102&rep=rep1&type=pdf
class Water :
	public GameObject
{
public:
	Water(std::string const& name, GameObject* parent = nullptr);
	~Water();

	virtual void Initialize() override;
	virtual void Update() override;

	/// Create and load the terrain mesh.
	/// tesselationLevel represent the number of square on an edge.
	bool LoadMesh(unsigned int  tesselationLevel);

	/// Compute the base (at time 0) phase and amplitude values of the waves 
	/// and store the result in the specified textures.
	/// h0 stores the h0(K) values.
	/// opph0 stores the h0*(K) values.
	/// cf paper for more details.
	//->void ComputeBasePhaseAmplitudes(GLuint h0, GLuint oppH0);

	/// Compute the water height map and register it into the map texture.
	//->void ComputeHeightMap(double t, GLuint map);

	//->virtual void Render(Shader const* shader, int pass) override;

	/// Draw the terrain in wireframe.
	RVAR(bool, Wireframe, false);

	/// The main color of the water.
	RVAR(glm::vec4, Color, glm::vec4(0, 0, 1, 1));

	/// The main color of the water.
	RVAR(double, Roughness, 0.1);

	/// The size of any edge. The mesh will be updated accordingly.
	RVAR(double, Size, 20.0);
	/// The height for the value 1 of the generated wave heightmap.
	RVAR(double, Height, 2000.0);

	/// The original tesselation level.
	/// The index means the number of squares of any edge,
	/// so a value of 10 means 100 squares in total.
	RVAR(int, OriginalTesselationLevel, 10);

	/// The tessellation ratio. Decide the amount of tessellation done to the object.
	/// The tessellation is in tessellation value/projected line pixels
	RVAR(double, TessellationRatio, 50);

	/// The tiling of the heightmap.
	RVAR(int, HeightMapTiling, 1);
	RVAR(double, WaterTileSize, 200);

	/// The size of the heightmap.
	RVAR(int, HeightMapSize, 512);

	/// The precision of the normals sampled from the height map.
	/// Correspond to the normalized coordinate offset used to 
	/// probe the heightmap multiplied by 1000.
	RVAR(double, NormalPrecision, 5);

	/// The vectorial foxrce of the wind.
	/// Influence the waves on the water surface.
	/// vector z component is ignored.
	RVAR(glm::vec3, WindForce, glm::vec3(31, 0, 0));

	/// Wave height factor.
	RVAR(double, WaveHeightFactor, 20.0);

private:
	void GenWavesData();
	void PrepareWaveData();
	void LoadRenderer();

	//->GLuint _heightMap;

	///// Store the h0(K) values.
	//GLuint _phaseAmplitudeMap;
	///// Store the h0*(K) values.
	//GLuint _oppositePhaseAmplitudeMap;
	/// A gaussian noise texture used for the generation of the base waves.
	//->GLuint _gaussianNoiseTexture;
	///// Store the complex values ready for inverse FFT.
	//GLuint _frequencyDomainBuffer;
	/// Store the base frequency values of the waves.
	//->GLuint _baseFrequencyBuffer;
	BaseFrequencyDataItem* _baseFrequencyData = nullptr;

	/// Used to check if the buffer is ready to be mapped and used by the CPU.
	bool _frequencyDomainBufferReady = false;

	Shader* _computeBaseWaveShader = nullptr;
	Shader* _computeHeightMapShader = nullptr;
};

