#pragma once

#include "Object.hpp"

#include "glm/glm.hpp"

class Camera :
	public Object
{
public:
	Camera(std::string const& name);
	~Camera();

	glm::mat4 GetProjection() const;

	/// Returns the view and the projection matrixes of the camera.
	void GetViewProjection(glm::mat4& view, glm::mat4& projection) const;

	glm::vec3 pos = glm::vec3(0.0f, 4.0f, 30.0f);
	glm::vec3 front = glm::vec3(0.0f, 0.0f, -1.0f);
	glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f);
	double yaw = -90.0;
	double pitch = 0.0;

	RVAR(double, FogDensity, 0.2);
	RVAR(glm::vec4, FogColor, glm::vec4(0.f, 0.f, 1.f, 1.f));
	RVAR(double, FOV, 45.0);
	RVAR(double, Near, 0.1);
	RVAR(double, Far, 1000.0);
	RVAR(double, Exposure, 1.0);
};