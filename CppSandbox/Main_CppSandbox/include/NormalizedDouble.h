#pragma once

#include "RuntimeVariable.hpp"

#include <string>

class NormalizedDouble :
	public RuntimeVariable
{
public:
	NormalizedDouble(std::string const& name, double value);
	~NormalizedDouble();
};

