#pragma once

#define SYSTEM System::GetInstance()

class System
{
public:
	System(System const&) = delete;
	System(System&&) = delete;
	~System() = default;

	System& operator=(System const&) = delete;
	System&& operator=(System&&) = delete;

	void Initialize() {};
	void Shutdown() {};

	void AskCloseApplication() { _applicationShouldClose = true; };
	bool ApplicationShouldClose() { return _applicationShouldClose; };

	static System& GetInstance();

private:
	System() = default;
	bool _applicationShouldClose = false;
};

