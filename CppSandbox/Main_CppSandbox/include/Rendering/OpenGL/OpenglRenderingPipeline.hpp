#pragma once

#include <vector>
#include <list>
#include <functional>

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "glm/glm.hpp"

#include "Rendering/RenderingPipeline.hpp"

class Material;
class Shader;
class OpenGLShader;
struct GLFWwindow;
class Camera;
class DirectionalLightGO;

class OpenGLRenderingPipeline
:	public RenderingPipeline
{
public:
	OpenGLRenderingPipeline();
	~OpenGLRenderingPipeline();

	virtual void Initialize() override;
	virtual void Shutdown() override;
	virtual void Render(RenderingContext* window, bool debugRender) override;

private:

	/// Setup the cameraNear and cameraFar floats in the shader.
	void AddCameraNearFarInfo(OpenGLShader const& shader, Camera const& camera) const;

	/// Setup the lights in the shader.
	/// Will use the texture 20 for shadow map informations, 
	/// and setup some uniform locations (cf implementation).
	void AddLightsInfo(OpenGLShader const& shader) const;

	void ComputeLightShadowMap	(	DirectionalLightGO const&			dLight, 
									std::list<Material*> const&		shadowedMaterials
								)	const;

	/// For each material, bind the shader for this pass, call the predraw func, 
	/// then call the material draw function for this pass.
	/// Every material with a valid shader will have the draw function called,
	/// but in not necessarily in the vector order.
	void DrawMaterials	(	std::vector<Material*> const&						materials, 
							std::function<void(Material const&, OpenGLShader const&)>	preDrawFunc,
							int													passIndex = 0
						)	const;

	void RenderSpecular	(	std::vector<Material*> const&	materials, 
							Camera const&					camera, 
							GLuint							skyboxTexture
						)	const;

	void RenderCubeMap	(	std::vector<Material*> const&	materials, 
							Camera const&					camera
						)	const;

	void RenderSpline	(	std::vector<Material*> const&	materials, 
							Camera const&					camera
						)	const;

	void RenderPatch	(	std::vector<Material*> const&	materials, 
							Camera const&					camera
						)	const;

	void RenderTerrain	(	std::vector<Material*> const&	materials, 
							Camera const&					camera
						)	const;

	void RenderWater	(	std::vector<Material*> const&	materials, 
							Camera const&					camera, 
							GLuint							skyboxTexture
						)	const;

	GLuint framebuffer;
	GLuint colorAttachment;
	//ds = depth stencil
	GLuint dsAttachment;
	GLuint dummyVAO;

	Shader const* worldPostProcessShader = nullptr;
};

