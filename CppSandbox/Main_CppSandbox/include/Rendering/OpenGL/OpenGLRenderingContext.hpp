#pragma once

#include "Rendering/RenderingContext.hpp"

struct GLFWwindow;

class OpenGLRenderingContext :
	public RenderingContext
{
public:
	OpenGLRenderingContext();
	~OpenGLRenderingContext();

	virtual void Initialize(RenderingContextDesc const& desc) override;
	virtual void Shutdown() override;

	GLFWwindow* GetGLFWWindow() { return _window; }
	static GLFWwindow* sGetGLFWWindow();

	static void ErrorHandle(int error, const char* description);

private:
	GLFWwindow* _window = nullptr;
};

