#pragma once
#include "Rendering/Lights/ShadowMap.hpp"

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>

class OpenGLShadowMap :
	public ShadowMap
{
public:
	OpenGLShadowMap();
	~OpenGLShadowMap();

	virtual void InitDepthMap() override;

	GLuint GetFramebuffer() { return _framebufferMap; }
	GLuint GetDepthMap() { return _depthMap; }

private:
	GLuint _framebufferMap;
	GLuint _depthMap;
};

