#pragma once
#include "Rendering/Resources/Mesh.hpp"

#include <cstdint>

#include "GL/glew.h"

class OpenGLMesh :
	public Mesh
{
public:
	OpenGLMesh();
	~OpenGLMesh();

	GLuint GetIBO() const { return _ibo; }
	GLuint GetVBO() const { return _vbo; }

	virtual void LoadData(Vertex* data, size_t count) override;
	virtual void LoadIndices(uint32_t* indices, size_t count) override;

protected:
	void SetIBO(GLuint ibo) { _ibo = ibo; }
	void SetVBO(GLuint vbo) { _vbo = vbo; }

private:
	GLuint _vbo = 0;
	GLuint _ibo = 0;
};

