#pragma once

#include "Rendering/Resources/Texture.hpp"

#include "GL/glew.h"

class OpenGLTexture
	: public Texture
{
public:
	OpenGLTexture();

	virtual bool			LoadImage(std::string const & path, bool linearSpace, bool isHDR = false) override;
	virtual bool			LoadImage(void * data, int width, int height, bool linearSpace, bool isHDR = false) override;
	virtual bool			LoadCubemap(std::string const & path) override;

	virtual AsyncLoadResult	PrepareImageAsync(std::string const & path, int * textureXY) override;
	virtual bool			LoadPreparedImageAsync(int width, int height, bool linearSpace) override;
	virtual void			DropLoadAsync() override;

	GLuint					GetTexture() { return _texture; }

private:
	void SetIsHDR(bool isHDR) { _isHDR = isHDR; }
	bool IsHDR() const { return _isHDR; }

	bool _isHDR = false;

	GLuint _texture;
};
