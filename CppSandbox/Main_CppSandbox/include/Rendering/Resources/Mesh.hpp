#pragma once


#include <cstdint>
#include <cstddef>

struct Vertex
{
	float vertex[3];
	float normal[3];
	float color[4];
	float textureCoord[2];
	float tangent[3];
};

/// Represent a mesh resource loaded in memory.
/// Will automatically generate and delete the default buffers.
class Mesh
{
public:
	Mesh();
	virtual ~Mesh();

	unsigned int GetVerticesCount() const { return _verticesCount; }
	unsigned int GetIndicesCount() const { return _indicesCount; }

	virtual void LoadData(Vertex* data, size_t count) = 0;
	virtual void LoadIndices(uint32_t* indexes, size_t count) = 0;

protected:
	void SetVerticesCount(unsigned int newVerticesCount) { _verticesCount = newVerticesCount; }
	void SetIndicesCount(unsigned int newIndexCount) { _indicesCount = newIndexCount; }

	size_t _verticesCount = 0u;
	size_t _indicesCount = 0u;
};

