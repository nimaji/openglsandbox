#pragma once

#include <string>
#include <vector>
#include <list>

#include "Utils.hpp"

class Material;

struct RenderingContextDesc
{
public:
	std::string		Name;
	unsigned int	Width;
	unsigned int	Height;
};

/// The RenderingContext class is used to provide a
/// platform agnostic interface to platform-specific 
/// implementations of a context for rendering purpose.
/// It provides tools for the context creation and
/// generic informations about the created context.
class RenderingContext
{
public:
	/// Initialize the context to enable rendering by the
	/// rendering pipeline possible.
	/// Also initialize informations about the current context.
	/// desc is used as an hint to the context creation.
	virtual void				Initialize(RenderingContextDesc const& desc) = 0;
	/// Shutdown the context and free all its data.
	virtual void				Shutdown() = 0;

	static bool					sIsInitialized();
	static unsigned int			sGetWidth();
	static unsigned int			sGetHeight();
	/// Set the current default context. This is this context that is used by the static fonctions.
	static void					sSetCurrent(RenderingContext* context) { _currentContext = context; };
	static RenderingContext*	sGetContext() { return _currentContext; };

	bool						IsInitialized() const { return _initialized; }
	unsigned int				GetWidth() const { return _width; }
	unsigned int				GetHeight() const { return _height; }

	void						RegisterMaterial(Material* newMaterial);
	bool						UnregisterMaterial(Material* material);
	bool						IsMaterialRegistered(Material* material);

	std::list<Material*> const&	GetMaterials() { return _materials; };

	template<typename T, typename Ret>
	std::vector<Ret*>			GetMaterialsOfType() { return Utils::GetObjectsOfType<T, Ret>(_materials.begin(), _materials.end()); };
protected:
	bool						_initialized = false;
	unsigned int				_width = 0;
	unsigned int				_height = 0;

private:
	static RenderingContext*	_currentContext;
	std::list<Material*> _materials;
};

