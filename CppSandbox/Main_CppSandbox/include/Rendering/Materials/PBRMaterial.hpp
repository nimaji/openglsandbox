#pragma once

#include "Rendering/Material.hpp"

#ifdef _WIN32
#include "Rendering/Materials/OpenGL/OpenglPBRMaterial.hpp"
#elif _ORBIS
#include "Rendering/Materials/PS4/PS4PBRMaterial.hpp"
#else
Error: Plateform not implemented.
#endif
