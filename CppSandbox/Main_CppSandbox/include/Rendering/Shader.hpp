#ifndef SHADER_HPP
#define SHADER_HPP

enum struct ShaderType
{
	Vertex = 0,
	Fragment,
	Compute,
	TessControl,
	TessEvaluation,
	Geometry
};

#ifdef _WIN32
#define PLATEFORM_SHADER OpenGLShader
#elif _ORBIS
#define PLATEFORM_SHADER PS4Shader
#else
Plateform not implemented
#endif

class DrawContextData;

/// Represent an shader program.
/// Used to compile and link different shaders to be used 
/// as a GPU programmed pipeline.
class Shader
{
public:
	Shader() = default;

	/// Compile shaders objects from the specified 
	/// vertex and fragment shader files.
	/// If compile auto is set, compile and link the shaders into a usable program.
	void Initialize(const char* vertexPath, const char* fragmentPath, bool compileAuto = true);

	/// Compile a compute shader object from the specified file,
	/// and link it as a usable program.
	void Initialize(const char* computeShader);

	static Shader* CreateShader(const char* vertexPath, const char* fragmentPath, bool compileAuto = true);
	static Shader* CreateShader(const char* computeShader);
	static void DestroyShader(Shader const* shader);

	/// Compile a shader present at the specified path and store it as the
	/// shaderType shader.
	/// You still need to compile the program to link the shaders.
	virtual bool LoadShader(const char* path, ShaderType shaderType) = 0;

	/// Compile and link the shaders into a usable program.
	virtual void Compile() = 0;
	/// Set the shader program as the currently used one.
	virtual void Use(DrawContextData* contextData) const = 0;
};

#endif

